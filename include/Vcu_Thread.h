#ifndef __VCU_THREAD_H
#define __VCU_THREAD_H

#include <stdbool.h>
#include <stdint.h>
#include <ros/ros.h>
#include <serial/serial.h>
#include <std_msgs/String.h>
#include <std_srvs/Empty.h>
#include <string>
#include <fstream>
#include <thread>
#include "Rec_Queue.h"

class Vcu_Thread
{
private:
    Rec_Queue &queue;
    std::thread *_serial_rec_thread;
    serial::Serial _serial;
    bool Serial_Open_State = false;

    std::string port_name;
    int baudrate;

    // void SerialRec_Thread();
    bool Serial_Open();

public:
    Vcu_Thread( Rec_Queue &q);
    ~Vcu_Thread();

    bool GetOpenState();
    void SerialRec_Thread();
    void SerialSend_Thread();
};

#endif
