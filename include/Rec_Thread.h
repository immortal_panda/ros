#ifndef __REC_THREAD_H
#define __REC_THREAD_H

#include <stdbool.h>
#include <stdint.h>
#include <ros/ros.h>
#include <serial/serial.h>
#include <std_msgs/String.h>
#include <std_srvs/Empty.h>
#include <string>
#include <fstream>
#include <thread>
#include "Rec_Queue.h"

class Rec_Thread
{
private:
    ros::NodeHandle &node_handle;
    Rec_Queue &queue;
    std::thread *_serial_rec_thread;
    serial::Serial _serial;
    bool Serial_Open_State = false;

    std::string port_name;
    int baudrate;

    // void SerialRec_Thread();
    bool Serial_Open();

public:
    Rec_Thread(ros::NodeHandle &nh, Rec_Queue &q);
    ~Rec_Thread();

    bool GetOpenState();
    void SerialRec_Thread();
};

#endif
