#ifndef _COMMON_H
#define _COMMON_H

#include <unordered_map>
#include <vector>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "AIGCJson.hpp"
#include <tuple>
#include <map>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "seed_log_example.h"
#include <chrono>
#include "RobotSerial.hpp"
#include <boost/thread.hpp>
#include <boost/crc.hpp>
#include "mqtt_receive_class.h"
#include <boost/format.hpp>



#include "Robot.h"
#include "Vcu_Thread.h"

using namespace std;
using namespace aigc;



enum TaskType
{
    TaskType_Start,
    TaskType_Pause,
    TaskType_Stop
};



/* 用户标准协议，里程计数据帧结构 OdomFrame 0x0405*/
typedef union
{
    uint8_t Data[4];
    struct
    {
        uint64_t RTK_Connect_Error                          : 1;
        uint64_t Three_Line_Radar_Connect_Error             : 1;
        uint64_t Four_Line_Radar_Connect_Error              : 1;
        uint64_t Can_Line_Connect_Error                     : 1;
        uint64_t RTK_GetLocation_Error                      : 1;
        uint64_t Three_Line_Radar_Data_Error                : 1;
        uint64_t Four_Line_Radar_Data_Error                 : 1;
        uint64_t LowPower                                   : 1;
        uint64_t LowDosage                                  : 1;
        uint64_t Deviation                                  : 1;
        uint64_t Reservation                                : 22;
    }bit;
} Robot_ErrorType;

class Gps_Data
{
public:
    double lat, lon;
    AIGC_JSON_HELPER( lat,lon) //成员注册
};

class Gps_Distance
{
public:
    int start_id;//起点id
    int end_id;//终点id 如果是最后一个点则为-1
    double distance;
    AIGC_JSON_HELPER( start_id,end_id,distance) //成员注册
};

class TaskInfo
{
public:
    int pathID;
    map<string,Gps_Data> waypoint;
    map<string,Gps_Distance> distance;
    AIGC_JSON_HELPER(pathID, waypoint,distance) //成员注册
    
};

class RobotTask
{
public:
    string CurrentTaskID;
    Gps_Data CurrentPos;
    string NextTaskID;
    Gps_Data NextPos;
    TaskInfo _taskInfo;
    AIGC_JSON_HELPER(CurrentTaskID,CurrentPos,NextTaskID,NextPos,_taskInfo) //成员注册
};

//
class TaskDataClass
{
    public:
        std::list<TaskInfo> tasks;
        AIGC_JSON_HELPER(tasks) //成员注册
};



/******************************************************
 * 系统参数设置
 * eg:
 * @param { string } imei:系统imei
 * @param { string } vx1000_com_port_name:RTK串口名字
 * @param { string } vcu_com_port_name: VCU串口名字     
 * @param { string } mqtt_username: mqtt 用户名  
 * @param { string } mqtt_pw: mqtt 密码   
 ******************************************************/
class SystemInfo 
{
    public:
        std::string imei;
        std::string vx1000_com_port_name;
        std::string vcu_com_port_name;
        std::string mqtt_server;
        std::string mqtt_username;
        std::string mqtt_pw;
        int RefreshFre;//刷新频率
    AIGC_JSON_HELPER(imei,vx1000_com_port_name,vcu_com_port_name,mqtt_server,mqtt_username,mqtt_pw,RefreshFre); //成员注册
};

class Common
{
public:
    static TaskDataClass _taskDataJson;
    static map<string,Gps_Data> navigation_data;
    static SystemInfo _system_setting;
    static RobotTask _RobotTask;
    static Robot_ErrorType _RobotError;
private:
    /* data */
public:
    template<typename T>
    static tuple<bool,T> hasDuplicateId(const std::list<T>& lst, const int& id);
    static string GetStringFormTimestamp(u_int64_t _time);
    static string GetCurTimeString();
    static time_t GetTimeStampFormString(string timeStr);
    static time_t GetCurTimeStamp();
    static uint16_t calculateModbusCRC(const unsigned char* buffer, size_t bufferSize);
    static bool isBitSet(unsigned char byte, int bitPosition);

    static bool isFileExists(string filePath);

    template<typename T>
    static T GetLocationJson(const char* jsonPath)
    {
        std::ifstream file(jsonPath); // 打开文件
        std::stringstream buffer;
        buffer << file.rdbuf(); // 读取文件内容到字符串流
        file.close(); // 关闭文件
        T obj;
        JsonHelper::JsonToObject(obj, buffer.str());
        return obj;
    }
 
    template<typename T>
    static void SaveJsonToLocation(T obj,string jsonPath)
    {
        string _json;
        JsonHelper::ObjectToJson(obj, _json);
        //写json文件
        std::ofstream file(jsonPath);
        file<<_json;
        file.close();
    }

    static void GetSystemJson(string jsonPath);
    
    static void SaveSystemJson( SystemInfo obj,string jsonPath);
    

    
    static double getAngle(const Gps_Data& position1, const Gps_Data& position2);
    static std::vector<Gps_Data> getFillPoints(const Gps_Data& sp, const Gps_Data& ep, double d, double r, bool c);
    static double MygetDistance(const Gps_Data& sp, const Gps_Data& ep);
    static std::list<double> getDistanceList(const std::list<Gps_Data>& points);
    static std::list<double> getDistanceList(const Gps_Data& point, const std::list<Gps_Data>& points);
    static std::list<double> getAdjacentSumList(const std::list<double>& distances);
    static double GetRad(double d);
    static std::list<int> getNearbyIndexList(const Gps_Data& point, const std::list<Gps_Data>& points, double distance);
    static std::tuple <bool, double> isYaw(const Gps_Data& point,const Gps_Data& startPoint,const Gps_Data& endPoint,double distance);
};
#endif
