#ifndef _ROBOT_H
#define _ROBOT_H

#include "Common.h"
#include "Rec_Queue.h"
#include "PIDController.hpp"


#define VCU_PROTO_LEN	13

#define VCU_FRAME_HEAD_0 0x55
#define VCU_FRAME_HEAD_2 0x04

#define VCU_TYPE_1 0x00   
#define VCU_TYPE_3 0x03
#define VCU_TYPE_5 0x05
#define VCU_TYPE_6 0x06

#define VCU_BUFF_SIZE 13*500 



/* 用户标准协议，VCU 数据帧结构 0x0400 */
typedef union
{
    uint8_t Data[8];
    struct
    {
        /* Byte Segment 0 */
        uint64_t leftWheel              : 16;
        uint64_t rightWheel             : 16;
        uint64_t mpump                  : 4;
        uint64_t swingArm               : 4;
        uint64_t emergency              : 1;
        uint64_t mainLight              : 1;
        uint64_t leftLight              : 1;
        uint64_t rightLight             : 1;
        uint64_t alert                  : 1;
        uint64_t Status_reservation     : 3;
        uint64_t mfLeft                 : 1;
        uint64_t mfUp                   : 1;
        uint64_t mfRight                : 1;
        uint64_t mPush                  : 1;
        uint64_t mfFan                  : 1;
        uint64_t autoSpraying           : 1;
        uint64_t automow                : 1;
        uint64_t action_reservation     : 1;

        
    } bit;
} VCU_400_TypeDef;

/* 用户标准协议，VCU 数据帧结构 0x0403 */
typedef union
{
    uint8_t Data[8];
    struct
    {
        /* Byte Segment 0 */
        uint64_t deviceCode     : 32;
        uint64_t errorCode      : 32;
    } bit;
} VCU_403_TypeDef;


/* 用户标准协议，VCU 数据帧结构 0x0406 */
typedef union
{
    uint8_t Data[8];
    struct
    {
        /* Byte Segment 0 */
        uint64_t hbc      : 8;
        uint64_t hbv      : 8;
        uint64_t hbpc     : 8;
        uint64_t hbcc     : 8;
        uint64_t lbc      : 8;
        uint64_t lbv      : 8;
        uint64_t lbpc     : 8;
        uint64_t lbcc     : 8;
    } bit;
} VCU_406_TypeDef;

class Robot
{
private:
    Rec_Queue &queue;
    bool match = true;
    void Decode();

    VCU_400_TypeDef m_400_Type;
    VCU_400_TypeDef m_401_Type;
    VCU_403_TypeDef m_403_Type;
    VCU_406_TypeDef m_406_Type;
    // 唯一的单例对象
    static Robot *instance_;
    ReportData _Robot_Data_From_Vcu;//从串口接收的数据
    int leftWheel_;
    int rightWheel_;//
    int setAngle_Speed;//角速度设置值
    bool b_pid_start = false;   
    // 创建一个PID控制器对象
    
    double setAngle = 0;
    double feedback=0;
    double dt=0.1;
    double Robot_diameter=0.9;//机器人直径
public:
    Robot(Rec_Queue &q);
    static Robot* getInstance();
    
    ~Robot();

    int Proto_INSP_UnPack_Robot_RxChar(uint8_t* buf);
    void Parase();

    NavigationData _Navigation_Data;
    ReportData _Server_Robot_Data;//服务器下发的数据
    int _Task_Type=2;
    ReportData GetReportData();

    void SendDataToVcu();
    void SportsStrategy();

public:
    uint8_t sendbuffer[13];
    int max_leftWheel_;
    int max_rightWheel_;
    int max_angular_speed;
};
#endif