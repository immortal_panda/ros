//
// Created by lin on 2024/3/13.
//
#ifndef PROJECT_SEED_LOG_EXAMPLE_H
#define PROJECT_SEED_LOG_EXAMPLE_H

#endif //PROJECT_SEED_LOG_EXAMPLE_H
/*
@filename: seed_log_example.h
@comment:
      log example
*/
#ifndef SEED_LOG_EXAMPLE_H
#define SEED_LOG_EXAMPLE_H
#include "seed_log_api.h"


//懒汉模式
static SeedLogSimpleManager& s_instance = SeedLogSimpleManager::instance();

/*
@function name : LOG宏定义，方便调用
*/
#define SEED_EXAMPLE_LOG0(level,message)\
    if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message);}

#define SEED_EXAMPLE_LOG1(level,fmt,v1)\
    if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,fmt,std::move(v1));}

#define SEED_EXAMPLE_LOG2(level,message,v1,v2)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message,v1,v2);}

#define SEED_EXAMPLE_LOG3(level,message,v1,v2,v3)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message,v1,v2,v3);}

#define SEED_EXAMPLE_LOG4(level,message,v1,v2,v3,v4)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message,v1,v2,v3,v4);}



class seed_log_example
{
public:
    static SD_ULONG32 seed_log_test();
    static SD_ULONG32 seed_log(std::string str);
    static SD_ULONG32 seed_wlog(std::wstring str);
    static std::string wstring_to_string(std::wstring  wstr);
};
#endif
