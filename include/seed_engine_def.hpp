//
// Created by lin on 2024/3/13.
//

/*
@filename: seed_engine_def.h
@comment:
	屏蔽系统和编译器差异，
	公共宏定义，
	注： 1.支持linux和windows 系统
		 2.cpu只支持32bit以上的架构
*/
#ifndef SEED_EINGIE_DEF_HPP
#define SEED_EINGIE_DEF_HPP

#ifdef WIN32
#ifdef _WINDLL
		#define SEED_API __declspec(dllexport)
	#else
		#define SEED_API __declspec(dllimport)
	#endif
#else
#define SEED_API
#endif


#ifndef INTERFACE
#define INTERFACE   struct
#endif

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#ifndef INOUT
#define INOUT
#endif

#ifndef SD_CHAR
#define SD_CHAR			  char
#endif

#ifndef SD_VOID
#define SD_VOID			  void
#endif


#ifndef SD_ULONG8
#define SD_ULONG8          unsigned char
#endif

#ifndef SD_LONG8
#define SD_LONG8          char
#endif

#ifndef SD_ULONG16
#define SD_ULONG16          unsigned short
#endif

#ifndef SD_LONG16
#define SD_LONG16           short
#endif

#ifndef SD_ULONG32
#define SD_ULONG32          unsigned int
#endif

#ifndef SD_LONG32
#define SD_LONG32          int
#endif

#ifndef SD_ULONG64
#define SD_ULONG64          unsigned long long
#endif

#ifndef SD_LONG64
#define SD_LONG64           long long
#endif

#ifndef SD_BOOL
#define SD_BOOL            int
#endif

#ifndef SD_FALSE
#define SD_FALSE           0
#endif

#ifndef  SD_TRUE
#define  SD_TRUE           1
#endif

#ifdef WIN32
#define  BP_Sleep(seconds)  Sleep((seconds)*1000)
#define  BP_MSleep(seconds) Sleep((seconds))
#else
#include <unistd.h>
#define  BP_Sleep(seconds)  sleep((seconds))
#define  BP_MSleep(seconds) usleep((seconds)*1000)
#endif


#ifndef WIN32
#define  BOOST_ALL_DYN_LINK
#endif

#ifdef WIN32
#define MFILENAME(x) strrchr(x,'\\')?strrchr(x,'\\')+1:x
#else
#define MFILENAME(x) strrchr(x,'/')?strrchr(x,'/')+1:x
#endif

#ifdef WIN32

#pragma warning(disable:4996)

#endif

enum BASE_ERROR_CODE
{
    ERROR_BASE_SUCCEED=0,
    ERROR_BASE_FALT,
    ERROR_BASE_INVALID
};
#endif
