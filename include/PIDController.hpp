//PID控制器
#ifndef _PIDCONTROLLER_HPP_
#define _PIDCONTROLLER_HPP_
#include <iostream>
class PIDController {
public:
    PIDController(double kp, double ki, double kd)
            : kp_(kp), ki_(ki), kd_(kd), integral_(0.0), previous_error_(0.0) {}

    /**
    * @brief 计算PID控制器的输出
    * @param setpoint 设定值
    * @param feedback 反馈值
    * @param dt 时间间隔
    * @return
    */
    double compute(double setpoint, double feedback, double dt) {
        double error = setpoint - feedback; // 误差，比例项使得控制系统能够迅速响应并逼近设定值
        integral_ += error * dt; // 累积误差，积分项用于补偿系统的稳态误差，即长时间内无法通过比例项和微分项完全纠正的误差
        double derivative = (error - previous_error_) / dt; // 误差的导数，微分项帮助控制系统更快地响应变化，并减小超调和震荡
        double output = kp_ * error + ki_ * integral_ + kd_ * derivative; // 控制量计算
        previous_error_ = error;
        return output;
    }

private:
    double kp_;
    double ki_;
    double kd_;
    double integral_;
    double previous_error_;
};
#endif