#pragma once
#include "AIGCJson.hpp"
#include <string>
using namespace std;

class Mqtt_KeepLive 
{
public:
    list<string> keepLive;
    u_int64_t timeStamp;

    AIGC_JSON_HELPER(keepLive, timeStamp) //成员注册
};

/******************数据基础包*****************/
class Mqtt_BasePackage
{
public:
    string msgType;//服务器给什么产品消息
    string imei;//产品IMEI
    int type;//消息类型
    u_int64_t timeStamp;//时间戳

    AIGC_JSON_HELPER(msgType, imei,type,timeStamp) //成员注册
    
};

/******************服务器任务*****************/
class TaskData
{
    public:
        int pathID; //路线ID
        string waypoint;//任务点数据
    AIGC_JSON_HELPER(pathID, waypoint) //成员注册
    
};

class Mqtt_Task:public Mqtt_BasePackage
{
    public:
        TaskData data;
    AIGC_JSON_HELPER(data) //成员注册
    AIGC_JSON_HELPER_BASE((Mqtt_BasePackage*)this) //基类注册
};


/******************服务器导航*****************/
class NavigationData
{
    public:
        int taskID;//任务ID
        int endStrategy;//任务执行策略 0：单趟执行 1：1次往返 N：n次往返
        int direction;//航线方向 0：正向 1：反向
        int navStrategy;//决策手段 1：遇障碍绕行 2：遇障碍停止
        float navSpeed;//导航线速度0.2-1.5
        float navYaw;//导航角速度30-80
        int lowPower;//停止电量 10-100
        int lowMedicine;//停止药量 10-100
    AIGC_JSON_HELPER(taskID, endStrategy,direction,navStrategy,navSpeed,navYaw,lowPower,lowMedicine) //成员注册
};

class Mqtt_Navigation:public Mqtt_BasePackage
{
    public:
        NavigationData data;
    AIGC_JSON_HELPER(data) //成员注册
    AIGC_JSON_HELPER_BASE((Mqtt_BasePackage*)this) //基类注册
};

/******************服务器控制*****************/
class VcuData
{
    public:
        float   leftWheel;//左轮速度
        float   rightWheel;//右轮速度
        int     mpump;//药泵控制0-10
        int     swingArm;//摆臂控制0-10
        int     emergency;//急停按钮0/1
        int     mainLight;//照明灯0/1
        int     leftLight;
        int     rightLight;
        int     alert;//声光报警
        int     mfLeft;//喷药-左
        int     mfUp;//喷药-上
        int     mfRight;//喷药-右
        int     mPush;//药箱泄流
        int     mfFan;//喷药风机
        int     autoSpraying;//自动喷药使能
        int     automow;//自动割草
        float   mfFRate;//实时流量
        float   mfCapacity;//药箱容量
        int     errorCode;//故障代码
    AIGC_JSON_HELPER(leftWheel, rightWheel,mpump,swingArm,emergency,mainLight,leftLight,rightLight,alert,mfLeft
    ,mfUp,mfRight,mPush,mfFan,autoSpraying,automow,mfFRate,mfCapacity,errorCode) //成员注册
};

class Obligate
{
    public:
        int obligate1;
        int obligate2;
        int obligate3;
        int obligate4;
        int obligate5;
        int obligate6;
        int obligate7;
        int obligate8;
    AIGC_JSON_HELPER(obligate1, obligate2,obligate3,obligate4,obligate5,obligate6,obligate7,obligate8) //成员注册
};

class CtrlData
{
public:
    double cmdV;//线速度
    double cmdR;//角速度
    VcuData vcuData;//控制命令
    Obligate obligate;//预留
    AIGC_JSON_HELPER(cmdV, cmdR,vcuData,obligate) //成员注册
};

class Mqtt_Ctrl:public Mqtt_BasePackage
{
    public:
        CtrlData data;
    AIGC_JSON_HELPER(data) //成员注册
    AIGC_JSON_HELPER_BASE((Mqtt_BasePackage*)this) //基类注册
};


/******************机器人上报数据*****************/

class ReportVcuData:public VcuData
{
    public:
        uint32_t     mode;//作业模式 1 喷药 2割草 3运输 其他无效
        uint32_t    errorCode;//32位故障码 16进制显示
        int         emergencyCode[8];//急停状态码
        uint32_t    deviceCode;//设备状态码
    AIGC_JSON_HELPER(mode, errorCode,emergencyCode[8],deviceCode) //成员注册   
    AIGC_JSON_HELPER_BASE((VcuData*)this) //基类注册
};


class ReportBmsData
{
    public:
        float       hbc;//高电压电池容量
        float       hbv;//高电压电池电压
        float       hbpc;//高电压电池放电电流
        float       hbcc;//高电压电池充电电流
        float       lbc;//低电压电池容量
        float       lbv;//低电压电池电压
        float       lbpc;//低电压电池放电电流
        //float       stateCode;//状态码
        float       lbcc;//状态码
        
    AIGC_JSON_HELPER(hbc, hbv,hbpc,hbcc,lbc,lbv,lbpc,lbcc) //成员注册   
    
};


class ReportData
{
public:
    double  vSpeed;//线速度 m/s 0-1
    double  rSpeed;//角速度 -10～10度
    string  lat;//纬度
    string  lon;//经度
    double  yaw;//航向角 正北0 正西90
    int     followNum;//前视点索引值
    int     navStrategy;//决策手段 1：遇障碍绕行 2：遇障碍停止
    int     taskTime;//导航任务实时累积时间
    double  obsX;//障碍物X值 车正前方为正
    double  obsY;//障碍物X值 车正左方为正
    double  obsDistance;//障碍物距离
    int     gpsStatus;//RTK状态 0：不可用 1单点定位 2差分定位  3无效PPS 4 实时差分定位 5RTK FLOAT 6 正在估算 7 定位模块断开
    int     status;//任务状态 500 系统初始化 501 空闲 502 遥控行走 503：执行任务中 504：正在加载航线 505：更换区域中 506：正在暂停 507：已暂停
                    //508：正在停止任务 509：任务完成
    int     taskID;//
    int     error;
    std::list<int>     emergencyCode;
    ReportVcuData  vcuData;//json格式
    ReportBmsData  bmsData;//json格式
    Obligate  obligate;
    AIGC_JSON_HELPER(vSpeed, rSpeed,lat,lon,yaw,followNum,navStrategy,taskTime,obsX,obsY,obsDistance,gpsStatus,status,taskID,error,emergencyCode,vcuData,bmsData,obligate) //成员注册  
};




class Mqtt_Report:public Mqtt_BasePackage
{
    public:
        ReportData data;//json形式上报
    AIGC_JSON_HELPER(data) //成员注册   
    AIGC_JSON_HELPER_BASE((Mqtt_BasePackage*)this) //基类注册
};

/******************服务器设置打点数据*****************/
class Mqtt_SetPoint:public Mqtt_BasePackage
{
    public:
        string data;//json形式上报
    AIGC_JSON_HELPER(data) //成员注册   
    AIGC_JSON_HELPER_BASE((Mqtt_BasePackage*)this) //基类注册
};

/******************机器人上报打点数据*****************/
class ReportPointData
{
public:
    string lat;//纬度
    string lon;//经度
    string timeStamp;//时间 “2022-10-21 18:05”
    AIGC_JSON_HELPER(lat, lon,timeStamp) //成员注册   
};



class Mqtt_ReportPoint:public Mqtt_BasePackage
{
    public:
        ReportPointData data;//json形式上报
    AIGC_JSON_HELPER(data) //成员注册   
    AIGC_JSON_HELPER_BASE((Mqtt_BasePackage*)this) //基类注册
};

/******************JSON数据结构体测试*****************/
class People 
{
public:
    string name;
    string age;

    AIGC_JSON_HELPER(name, age) //成员注册
};
class Student : People
{
public:
    string depart;
    int grade;
    AIGC_JSON_HELPER(depart, grade) //成员注册
    AIGC_JSON_HELPER_BASE((People*)this) //基类注册
};
class Class 
{
public:
    string name;
    int test;
    std::list<Student> students;
    std::unordered_map<std::string, int> property;
    People master;
    std::set<std::string> users;
    AIGC_JSON_HELPER(name, test, students, master, property, users) //成员注册
    AIGC_JSON_HELPER_DEFAULT("test=123")
};
