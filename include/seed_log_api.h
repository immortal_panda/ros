//
// Created by lin on 2024/3/13.
//
#ifndef SEED_LOG_H
#define SEED_LOG_H
/*
@filename: seed_log_api.h
@comment:
    head file of Log-interface for public
*/
#include "seed_engine_def.hpp"
#include <cstdarg>
#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>
// bytes of log in one line
#ifndef  MAXLOGLEN
#define  MAXLOGLEN     2048
#endif


enum SEED_LOG_LEVEL
{
    SD_LOG_DEBUG=0,
    SD_LOG_INFO,
    SD_LOG_WARNING,
    SD_LOG_ERROR,
    SD_LOG_KEY,
    SD_LOG_STOP
};
/*
@class name : SeedLogSimpleManager
*/
struct SeedLogCfg
{
    std::string m_path;
    std::string m_filename;
    SD_BOOL m_isDebug = SD_TRUE;
    SD_ULONG32 m_LogId = 0;
    //SD_ULONG32 m_ulFileSize = 10*1024*1024;
    //default 10M
    SD_ULONG32 m_ulFileSize = 10*1024*1024;
};

INTERFACE SEED_API SeedLogApi
{
/*
@function name : init
*/
static SD_ULONG32 LogSystemInit(SD_BOOL isconsole);
/*
@function name : init
*/
virtual SD_ULONG32 LogInit(IN SeedLogCfg cfgs) = 0;
/*
@function name : GetLogId
*/
virtual SD_ULONG32 GetLogId() = 0;
/*
@function name : GetLogId
*/
virtual SD_BOOL GetDebug() = 0;
/*
@function name : log
*/
virtual SD_VOID log(IN SD_ULONG32 logid, IN SD_BOOL debug, IN int level, IN const char* filename, IN SD_ULONG32 line, IN std::string& message) = 0;
/*
@function name : log
*/
virtual SD_VOID log(IN SD_ULONG32 logid, IN SD_BOOL debug, IN int level, IN const char* filename, IN SD_ULONG32 line, IN const char* message) = 0;
};
/*
@class name : Mutilple log-class
*/
class SEED_API SeedLogMutilpleManager
        {
                public:
                static SeedLogMutilpleManager& instance();
                /*
                @function name : init
                */
                static SD_ULONG32 LogSystemInit(SD_BOOL isconsole);
                /*
                @function name : log init;
                */
                SD_ULONG32 SeedLogMutilpleInit(IN std::vector<SeedLogCfg>& configs);
                /*
                @function name : log_write
                */
                static	SD_VOID log_write(IN SD_ULONG32 logid, IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, ...);
                /*
                @function name : callback_write
                */
                static	SD_LONG32 callback_write(IN SD_ULONG32 logid, IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, va_list va);
                private:
                /*
                @function name : log_write
                */
                SeedLogMutilpleManager(){}
                private:

                static SeedLogMutilpleManager * s_instance;

                std::map<SD_ULONG32, SeedLogApi*> m_logs;

                SD_ULONG32 m_adapter_id = 0;
        };

/*
@class name : simple model of log-model
*/
class SEED_API SeedLogSimpleManager
        {
                public:
                static SeedLogSimpleManager& instance();
                /*
                @function name : init
                */
                static SD_ULONG32 LogSystemInit(SD_BOOL isconsole);
                /*
                @function name : log init;
                */
                SD_ULONG32 SeedLogSimpleInit(IN SeedLogCfg config);
                /*
                @function name : log_write
                */
                static	SD_VOID log_write(IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, ...);
                /*
                @function name : callback_write
                */
                static	SD_LONG32 callback_write(IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, va_list va);
                private:
                /*
                @function name : log_write
                */
                SeedLogSimpleManager(){}
                private:

                static SeedLogSimpleManager * s_instance;

                SeedLogApi* m_log = nullptr;

                SeedLogCfg m_config;
        };

/*
@function name : LOG宏定义，方便调用
*/
#define SEED_LOG0(level,message)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message);}

#define SEED_LOG1(level,fmt,v1)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,fmt,v1);}

#define SEED_LOG2(level,message,v1,v2)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message,v1,v2);}

#define SEED_LOG3(level,message,v1,v2,v3)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message,v1,v2,v3);}

#define SEED_LOG4(level,message,v1,v2,v3,v4)\
	if(1){SeedLogSimpleManager::log_write(level,__FILE__,__LINE__,message,v1,v2,v3,v4);}

#endif

