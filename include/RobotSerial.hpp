#include <iostream>
#include <string>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "Common.h"
using namespace std;


class RobotSerialPort {
private:
    int fd; // 文件描述符
    std::string portName; // 串口名称
    struct termios tty;
    bool isOpen;
    // 设置串口参数
    void set_interface_attributes(int speed, int parity, int databits, int stopbits) {
        struct termios tty;
        if (tcgetattr(fd, &tty) != 0) {
            std::cerr << "Error from tcgetattr: " << errno << std::endl;
            throw std::system_error(errno, std::system_category());
        }
 
        cfsetispeed(&tty, speed);
        cfsetospeed(&tty, speed);
 
        tty.c_cflag |= (CLOCAL | CREAD); // 开启接收
        tty.c_cflag &= ~CSIZE; // 清除当前数据位设置
        tty.c_cflag |= databits; // 设置数据位数
 
        switch (parity) {
            case 'n':
            case 'N':
                tty.c_cflag &= ~PARENB; // 清除奇偶校验位
                tty.c_iflag &= ~INPCK; // 禁止奇偶校验
                break;
            // 其他情况设置奇偶校验位...
        }
 
        switch (stopbits) {
            case 1:
                tty.c_cflag &= ~CSTOPB; // 设置1位停止位
                break;
            case 2:
                tty.c_cflag |= CSTOPB; // 设置2位停止位
                break;
        }
 
        tty.c_cc[VMIN] = 1; // 读取最小字符数
        tty.c_cc[VTIME] = 5; // 读取最小字符时间
 
        if (tcsetattr(fd, TCSANOW, &tty) != 0) {
            std::cerr << "Error from tcsetattr: " << errno << std::endl;
            throw std::system_error(errno, std::system_category());
        }
    }
 
public:
    RobotSerialPort(std::string port)  {
        portName = port;
        //打开串口
        fd = open(portName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
        if (fd == -1) {
            std::cerr << "Error in opening tty port: " << portName << std::endl;
            SEED_EXAMPLE_LOG1(SD_LOG_ERROR,"无法打开串口%s",portName.c_str());
            return;
        }
 
        fcntl(fd, F_SETFL, 0); // 清除非阻塞标志
 
        set_interface_attributes(B115200, 'N', 8, 1); // 设置默认参数
        isOpen = true;
    }
    
    RobotSerialPort()
    {
        
    }

    ~RobotSerialPort() {
        closePort();
    }
    
    void closePort() {
        if (isOpen) {
            if (fd != -1) {
                close(fd);
                fd = -1;
            }
            isOpen = false;
        }
    }   

    bool isOpenPort() {
        return isOpen;
    }
    void set_baudrate(int speed) {
        set_interface_attributes(speed, 'N', 8, 1);
    }
 
    // 写入数据
    void write(const std::string& data) {
        if (::write(fd, data.c_str(), data.length()) != static_cast<ssize_t>(data.length())) {
            throw std::runtime_error("Error writing to serial port");
        }
    }

    bool writeData(const unsigned char* data, size_t length) {
        if (fd == -1) {
            std::cerr << "Error: Serial port is not open." << std::endl;
            return false;
        }

        ssize_t bytesWritten = ::write(fd, data, length);
        if (bytesWritten == -1) {
            std::cerr << "Error: Failed to write to serial port." << std::endl;
            return false;
        }

        return true;
    }

    bool readData(unsigned char* buffer, size_t bufferSize, ssize_t& bytesRead) {
        if (fd == -1) {
            std::cerr << "Error: Serial port is not open." << std::endl;
            return false;
        }

        bytesRead = ::read(fd, buffer, bufferSize);
        if (bytesRead == -1) {
            std::cerr << "Error: Failed to read from serial port." << std::endl;
            return false;
        }

        return true;
    }

    bool readData(unsigned char* buffer, size_t bufferSize, ssize_t& bytesRead, int timeoutSec, int timeoutUSec) 
    {
        if (fd == -1) {
            std::cerr << "Error: Serial port is not open." << std::endl;
            return false;
        }

        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(fd, &readfds);

        struct timeval timeout;
        timeout.tv_sec = timeoutSec;
        timeout.tv_usec = timeoutUSec;

        int result = select(fd , &readfds, NULL, NULL, &timeout);
        if (result == -1) {
            std::cerr << "Error: Failed to select from serial port: " << strerror(errno) << std::endl;
            return false;
        } else if (result == 0) {
            std::cerr << "Error: Timeout occurred while waiting for data." << std::endl;
            return false;
        }

        bytesRead = ::read(fd, buffer, bufferSize);
        if (bytesRead == -1) {
            std::cerr << "Error: Failed to read from serial port: " << strerror(errno) << std::endl;
            return false;
        }

        return true;
    }
 
    // 读取数据，最多读取size个字节，或者直到有换行符，或者超时
    std::string read(size_t size, int timeout_ms = -1) {
        std::string data;
        data.resize(size);

        struct timeval tv;
        fd_set read_fds;

        // 设置超时
        if (timeout_ms >= 0) {
            tv.tv_sec = timeout_ms / 1000;
            tv.tv_usec = (timeout_ms % 1000) * 1000;
        } else {
            // 无超时
            tv.tv_sec = tv.tv_usec = 0;
        }

        // 准备文件描述符集合
        FD_ZERO(&read_fds);
        FD_SET(fd, &read_fds);

        // 等待数据可读
        int select_result = select(fd + 1, &read_fds, nullptr, nullptr, timeout_ms >= 0 ? &tv : nullptr);
        if (select_result == -1) {
            if (errno != EINTR) {
                throw std::system_error(errno, std::system_category(), "Error waiting for data");
            }
        } else if (select_result == 0) {
            // 超时
            throw std::runtime_error("Timeout while reading data");
        }

        // 读取数据
        ssize_t n = ::read(fd, &data[0], size);
        if (n < 0) {
            throw std::runtime_error("Error reading from serial port");
        } else if (n < size) {
            // 调整字符串大小以匹配实际读取的字节数
            data.resize(n);
        }

        // 查找换行符并截断字符串（如果需要）
        size_t newline_pos = data.find('\n');
        if (newline_pos != std::string::npos) {
            data.resize(newline_pos + 1);
        }

        return data;
    }
};