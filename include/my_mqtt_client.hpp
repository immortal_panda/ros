
#include "mqtt/async_client.h"
#include "mqtt_receive_class.h"
#include "Common.h"
#include <boost/algorithm/string.hpp>
#include <map>
#include <tuple>
#include <algorithm>


using namespace std;
using message_handler = std::function<void(const std::string&,const std::string&)>;





class MqttClient {
public:
    static mqtt::async_client* client;
    static boost::thread roport_robot_data_thread;
    static bool isConnect;
public:
	
public:
    // MqttClient(const std::string& serverURI, const std::string& clientId)
    //     : client(serverURI, clientId) {
    //     connOpts.set_keep_alive_interval(20);
    //     connOpts.set_clean_session(true);
    // }

    

    static void connect(const std::string& serverURI, const std::string& clientId) {
        client = new mqtt::async_client(serverURI, clientId);
        mqtt::connect_options connOpts;
        try
        {
            /* code */
            connOpts.set_keep_alive_interval(20);
            connOpts.set_clean_session(true);
            connOpts.set_password(Common::_system_setting.mqtt_pw.c_str());
            connOpts.set_user_name(Common::_system_setting.mqtt_username.c_str());
            client->connect(connOpts)->wait();
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
            SEED_EXAMPLE_LOG1(SD_LOG_ERROR, "MQTT 连接失败%s",e.what());
            isConnect=false;
            return;
        }
        
        isConnect=true;
        SEED_EXAMPLE_LOG0(SD_LOG_INFO, "MQTT 连接成功");

        roport_robot_data_thread = boost::thread(RobotReportStatus);
    
        roport_robot_data_thread.detach();
    }

    // void connect() {
    //     try {
    //         client.connect(connOpts)->wait(
    //         );
    //         SEED_EXAMPLE_LOG0(SD_LOG_INFO, "MQTT 连接成功");
    //     }
    //     catch (const mqtt::exception& exc) {
    //         std::cerr << "Error: " << exc.what() << std::endl;
    //         SEED_EXAMPLE_LOG1(SD_LOG_ERROR, "MQTT 连接失败%s",exc.what());
    //     }
    // }

    static void disconnect() {
        try {
            client->disconnect()->wait();
            delete client;
            client = nullptr;
            SEED_EXAMPLE_LOG0(SD_LOG_INFO, "MQTT 断开连接成功");

            if (roport_robot_data_thread.joinable()) {
                //请求退出
                roport_robot_data_thread.interrupt();
                SEED_EXAMPLE_LOG0(SD_LOG_INFO, "请求退出线程");
            }
        }
        catch (const mqtt::exception& exc) {
            std::cerr << "Error: " << exc.what() << std::endl;
            SEED_EXAMPLE_LOG1(SD_LOG_ERROR, "MQTT 断开失败%s",exc.what());
        }
    }

    static void publish(const std::string& topic, const std::string& payload) {
        mqtt::message_ptr pubmsg = mqtt::make_message(topic, payload);
        pubmsg->set_qos(0);
        //client->publish(pubmsg)->wait();
        client->publish(pubmsg);
        //SEED_EXAMPLE_LOG2(SD_LOG_INFO, "MQTT 发送数据成功\r\n 话题%s,内容%s",topic.c_str(),payload.c_str());
    }

    static void subscribe(const std::string& topic,message_handler cb) {
        

        client->set_message_callback([cb](mqtt::const_message_ptr message) 
        {
            cb(message->get_topic(),message->get_payload_str());   //执行函数cb
        });

        //! Set connect options and do connect
        // auto opts = mqtt::connect_options_builder()
        //                 .mqtt_version(MQTTVERSION_5)
        //                 .clean_start(true)
        //                 .finalize();
        // client.connect(opts);

        client->subscribe(topic, 1)->wait();
        
    }

    

    static void unsubscribe(const std::string& topic) {
        client->unsubscribe(topic)->wait();
    }
    

    static void ParaseServerMessage(const std::string &topic,const std::string &data)
        {
            //先用base解析
            SEED_EXAMPLE_LOG2(SD_LOG_INFO, "MQTT 话题%s接收数据成功\r\n %s",topic.c_str(),data.c_str());
            //if (topic.c_str()=="SERVICE/ROBOT_KEEPLIVE")
            if (boost::algorithm::equals(topic.c_str(), "SERVICE/ROBOT_KEEPLIVE"))
            {
                
                Mqtt_KeepLive receive_keeplive;
	            JsonHelper::JsonToObject(receive_keeplive, data);
                try
                {
                    string _front = receive_keeplive.keepLive.front().c_str();
                    if (boost::algorithm::equals(_front, "0x22"))
                    {
                        /* code */
                        string _str=Common::GetStringFormTimestamp(receive_keeplive.timeStamp/1000);
                        SEED_EXAMPLE_LOG1(SD_LOG_INFO,"当前心跳时间%s",_str.c_str());
                        return;
                    }
                }
                catch(const std::exception& e)
                {
                    std::cerr << e.what() << '\n';
                    SEED_EXAMPLE_LOG0(SD_LOG_WARNING,"心跳包错误");
                }
            }
            else
            {
                Mqtt_BasePackage receive_package;
                JsonHelper::JsonToObject(receive_package, data);
                switch (int(receive_package.type))
                {
                case 1001:
                    ParaseTask(data);
                    break;
                case 1011:
                    ParaseNavigation_Start(data);
                    break;
                case 1012:
                    ParaseNavigation_Pause(data);
                    break;
                case 1013:
                    ParaseNavigation_Continue(data);
                case 1014:
                    ParaseNavigation_Stop(data);
                    break;
                case 1015:
                    ParaseRomoteCtrl(data);
                    break;
                case 1016:
                    ParaseSetPoint(data);
                    break;
                }
            }
            
        }

    static void ParaseTask(const std::string &data)
    {
        SEED_EXAMPLE_LOG0(SD_LOG_INFO,"任务点下发");
        
        Mqtt_Task task_package;
        JsonHelper::JsonToObject(task_package, data);
        string waypoint = task_package.data.waypoint;
        int task_id = task_package.data.pathID;
        //0 118.6542856 36.2587463,  //实例数据。用,分割
        vector<string> split_vector;
        boost::algorithm::split(split_vector, waypoint, boost::is_any_of(","));
        //清空数组
        Common::navigation_data.clear();
        list<Gps_Data> gps_list;
        for(auto it : split_vector)
        {
            vector<string> gps_data_vectors;
            boost::algorithm::split(gps_data_vectors, it, boost::is_any_of(" "));
            Gps_Data _temp;
            _temp.lon = std::stof(gps_data_vectors[1]);
            _temp.lat = std::stof(gps_data_vectors[2]);
            gps_list.push_back(_temp);
            Common::navigation_data.insert(std::make_pair(gps_data_vectors[0],_temp));
        }
        //调用js获取所有点之间的距离
        //list<double> 转化成vector<double>
        // SEED_EXAMPLE_LOG0(SD_LOG_INFO,"计算距离开始");
        
        

        
        // vector<double> distances;
        // for(auto it : _temp_dis_list)
        // {
        //     distances.push_back(it);
        // }
        // map<string,Gps_Distance> _distance;
        // //遍历distances
        // for(int i=0;i<distances.size();i++)
        // {
        //     //获取it的下标
        //     int index = i+1;
        //     //获取it的值
        //     Gps_Distance _temp;
        //     _temp.distance = distances[i];
        //     _temp.start_id = index;
        //     _temp.end_id = index+1;          
        //     string _key = std::to_string(index);
        //     _distance.insert(std::make_pair(_key.c_str(),_temp));
            
        // }
        // SEED_EXAMPLE_LOG0(SD_LOG_INFO,"计算距离结束");

        // 暂时不需要，因为只有一个任务
        // //删除重复任务
        // auto item = std::find_if(Common::_taskDataJson.tasks.begin(), Common::_taskDataJson.tasks.end(), [&](const TaskInfo& item) {
        //     return item.pathID == task_id;
        // });
        // if(item != Common::_taskDataJson.tasks.end())
        // {
        //     SEED_EXAMPLE_LOG1(SD_LOG_INFO, "删除重复任务,id=%d",(*item).pathID);
        //     Common::_taskDataJson.tasks.erase(item);
        // }
        //目前是全部删除。本地只有一个任务
        Common::_taskDataJson.tasks.clear();
        //加入新元素
        TaskInfo _taskinfo;
        _taskinfo.pathID = task_id;
        _taskinfo.waypoint = Common::navigation_data;
        
        
        Common::_taskDataJson.tasks.push_back( _taskinfo);
        
        Common::_RobotTask._taskInfo = _taskinfo;
        Common::_RobotTask.CurrentPos =Common::navigation_data.at("1");
        Common::_RobotTask.CurrentTaskID="1";
        Common::_RobotTask.NextTaskID="2";
        Common::_RobotTask.NextPos =Common::navigation_data.at("2");
        //Common::SaveRobotTaskJson(Common::_RobotTask,"task.json");
        Common::SaveJsonToLocation(Common::_RobotTask,"task.json");
        SEED_EXAMPLE_LOG0(SD_LOG_INFO,"任务点保存本地");
    }
    
    static void ParaseNavigation_Start(const std::string &data)
    {
        //开始导航
        Mqtt_Navigation navigation_package;
        JsonHelper::JsonToObject(navigation_package, data);
        Robot::getInstance()->_Navigation_Data = navigation_package.data;
        Robot::getInstance()->_Task_Type = TaskType_Start;
    }
    static void ParaseNavigation_Pause(const std::string &data)
    {
        Mqtt_Navigation navigation_package;
        JsonHelper::JsonToObject(navigation_package, data);
        Robot::getInstance()->_Task_Type = TaskType_Pause;
        
    }
    static void ParaseNavigation_Continue(const std::string &data)
    {
        Mqtt_Navigation navigation_package;
        JsonHelper::JsonToObject(navigation_package, data);
        Robot::getInstance()->_Task_Type = TaskType_Start;
    }
    static void ParaseNavigation_Stop(const std::string &data)
    {
        Mqtt_Navigation navigation_package;
        JsonHelper::JsonToObject(navigation_package, data);
        Robot::getInstance()->_Task_Type = TaskType_Stop;
    }

    static void ParaseRomoteCtrl(const std::string &data)
    {
        Mqtt_Ctrl ctrl_package;
        JsonHelper::JsonToObject(ctrl_package, data);
        //运动控制
        Robot::getInstance()->max_leftWheel_= ctrl_package.data.cmdV;
        Robot::getInstance()->max_rightWheel_ = ctrl_package.data.cmdR;
        Robot::getInstance()->max_angular_speed = ctrl_package.data.cmdR;
        Robot::getInstance()->_Server_Robot_Data.vcuData.mpump=ctrl_package.data.vcuData.mpump;
        Robot::getInstance()->_Server_Robot_Data.vcuData.swingArm=ctrl_package.data.vcuData.swingArm;
        Robot::getInstance()->_Server_Robot_Data.vcuData.emergency=ctrl_package.data.vcuData.emergency;
        Robot::getInstance()->_Server_Robot_Data.vcuData.mainLight=ctrl_package.data.vcuData.mainLight;
        Robot::getInstance()->_Server_Robot_Data.vcuData.leftLight=ctrl_package.data.vcuData.leftLight;
        Robot::getInstance()->_Server_Robot_Data.vcuData.rightLight=ctrl_package.data.vcuData.rightLight;
        Robot::getInstance()->_Server_Robot_Data.vcuData.alert=ctrl_package.data.vcuData.alert;
        Robot::getInstance()->_Server_Robot_Data.vcuData.mfLeft=ctrl_package.data.vcuData.mfLeft;
        Robot::getInstance()->_Server_Robot_Data.vcuData.mfUp=ctrl_package.data.vcuData.mfUp;
        Robot::getInstance()->_Server_Robot_Data.vcuData.mfRight=ctrl_package.data.vcuData.mfRight;
        Robot::getInstance()->_Server_Robot_Data.vcuData.mPush=ctrl_package.data.vcuData.mPush;
        Robot::getInstance()->_Server_Robot_Data.vcuData.mfFan=ctrl_package.data.vcuData.mfFan;
        Robot::getInstance()->_Server_Robot_Data.vcuData.autoSpraying=ctrl_package.data.vcuData.autoSpraying;
        Robot::getInstance()->_Server_Robot_Data.vcuData.automow=ctrl_package.data.vcuData.automow;

        
    }

    static void ParaseSetPoint(const std::string &data)
    {
        // //返回点位数据
        SEED_EXAMPLE_LOG0(SD_LOG_INFO,"打点请求");
        ReportPointData _report_data;
        _report_data.lat = Robot::getInstance()->GetReportData().lat;
        _report_data.lon = Robot::getInstance()->GetReportData().lon;
        //当前时间
        _report_data.timeStamp = Common::GetCurTimeString();
        Mqtt_ReportPoint _mqtt_report;
        
        _mqtt_report.imei = Common::_system_setting.imei;
        _mqtt_report.msgType = "robot";
        _mqtt_report.type=2003;
        _mqtt_report.data = _report_data;
        
        _mqtt_report.timeStamp = static_cast<u_int64_t>(Common::GetTimeStampFormString(_report_data.timeStamp));
        //json转化成string
        string _report_point_str;
        JsonHelper::ObjectToJson(_mqtt_report,_report_point_str);
        std::string str = (boost::format("SERVICE/ROBOT_%1%") % Common::_system_setting.imei.c_str() ).str();
        publish(str,_report_point_str);

        
        
    }

    static void RobotReportStatus()
    {
        while (isConnect) {
        SEED_EXAMPLE_LOG1(SD_LOG_INFO, "当前时间 %s",Common::GetCurTimeString().c_str());
        boost::this_thread::sleep(boost::posix_time::seconds(1));
        Mqtt_Report _report_status;
        _report_status.imei = Common::_system_setting.imei;
        _report_status.msgType = "robot";
        _report_status.type=1020;
        _report_status.timeStamp = static_cast<u_int64_t>(Common::GetCurTimeStamp());
        ReportData _data=Robot::getInstance()->GetReportData();
       
        _report_status.data = _data;
        string _report_status_str;
        string result_message;
        bool b_res=JsonHelper::ObjectToJson(_report_status,_report_status_str,&result_message);
        if (b_res)
        {
            publish("SERVICE/ROBOT_9257",_report_status_str);
        }
        else
            SEED_EXAMPLE_LOG1(SD_LOG_INFO, "json错误 %s",result_message.c_str());
        


  
        

        
        }
    }
};

