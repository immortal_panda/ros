//
// Created by lin on 2024/3/13.
//

#ifndef PROJECT_SEED_LOG_LOCAL_H
#define PROJECT_SEED_LOG_LOCAL_H

#endif //PROJECT_SEED_LOG_LOCAL_H
/*
@filename: seed_log_local.h
@comment:
       head file of specific code in model of log
*/
#ifndef SEED_LOG_LOCAL_H
#define SEED_LOG_LOCAL_H
#include "seed_engine_def.hpp"
#include "seed_log_api.h"
#include <vector>

template< typename CharT, typename TraitsT >
inline std::basic_ostream< CharT, TraitsT >& operator<< (
        std::basic_ostream< CharT, TraitsT >& strm, SEED_LOG_LEVEL lvl)
{
    static const char* const str[] =
            {
                    "debug",
                    "Info",
                    "Warning",
                    "Error",
                    "Key"
            };
    if (static_cast<std::size_t>(lvl) < (sizeof(str) / sizeof(*str)))
        strm << str[lvl];
    else
        strm << static_cast<int>(lvl);
    return strm;
}

class SeedLogLocal: public SeedLogApi
{
public:
    /*
    @function name : init
    */
    static SD_ULONG32 InitLogSystem(SD_BOOL isconsole);
    /*
    @function name : LogInit
    */
    SD_ULONG32 LogInit(IN SeedLogCfg cfgs) override;
    /*
    @function name : GetLogId
    */
    SD_ULONG32 GetLogId() override;
    /*
    @function name : GetLogId
    */
    SD_BOOL GetDebug() override;
    /*
    @function name : log
    */
    SD_VOID log(IN SD_ULONG32 logid, IN SD_BOOL debug, IN int level, IN const char* filename, IN SD_ULONG32 line, IN std::string& message) override;
    /*
    @function name : log
    */
    SD_VOID log(IN SD_ULONG32 logid, IN SD_BOOL debug, IN int level, IN const char* filename, IN SD_ULONG32 line, IN const char* message) override;
private:
    /*
    @function name : add_log_file
    */
    SD_ULONG32 add_log_file(IN SeedLogCfg cfg);

private:
    SeedLogCfg m_cfg;
};
#endif
