#ifndef __REC_QUEUE_H
#define __REC_QUEUE_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

typedef enum
{
    RecQueue_ok = 0,
    RecQueue_overflow_w,
    RecQueue_overflow_r,
    RecQueue_empty,
    RecQueue_full,
    RecQueue_obj_error,
} RecQueue_state;

typedef union
{
    uint16_t output;

    struct
    {
        RecQueue_state state : 8;
        uint8_t value : 8;
    } reg;
}RecQueue_CheckOut_u;

class Rec_Queue
{
private:
    bool init_state = false;
    
    uint16_t size = 0;  // current data size in queue
    RecQueue_state state;
    
    uint16_t end_pos = 0;
    uint8_t *buff = NULL;

    RecQueue_state UpdateState();

public:
    Rec_Queue(uint8_t *p_buf, uint16_t full_size);
    ~Rec_Queue();

    bool Reset();
    bool Check(uint16_t index, uint8_t *data, uint16_t size);
    RecQueue_state Push(uint8_t *data, uint16_t size);
    RecQueue_state Pop(uint8_t *data, uint16_t size);
    RecQueue_state GetState();
    uint16_t GetSize();
    uint16_t Head();
    uint16_t End();
    uint16_t head_pos = 0;
    uint16_t lenth = 0; // total queue size
};


#endif