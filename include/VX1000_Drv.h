#ifndef __VX1000_DRV_H
#define __VX1000_DRV_H

#include <ros/ros.h>
#include "Rec_Queue.h"
#include <stdint.h>
#include <stdbool.h>
#include <system_msgs/vx1000_ins.h>
#include <system_msgs/vx1000_imu.h>
#include <system_msgs/vx1000_gnss.h>
#include <system_msgs/vx1000_stat.h>
#include <system_msgs/vx1000_cov.h>
#include <system_msgs/vx1000_odom.h>
#include "std_msgs/Float64.h"

#define Vx1000_PROTO_LEN	68

#define VX1000_FRAME_HEAD_0 0xAA
#define VX1000_FRAME_HEAD_1 0x55

#define VX1000_TYPE_1 0xA0
#define VX1000_TYPE_2 0xA1
#define VX1000_TYPE_3 0xA2
#define VX1000_TYPE_4 0xA3
#define VX1000_TYPE_5 0xA4
#define VX1000_TYPE_6 0xA5
#define VX1000_TYPE_7 0xA6
#define VX1000_TYPE_8 0xA7

/* 用户标准协议，InsFrame 组合导航数据帧结构 0x0400 */
typedef union
{
    uint8_t Data[64];
    struct
    {
        /* Byte Segment 0 */
        uint64_t gps_week           : 16;
        uint64_t gps_week_second    : 32;
        uint64_t reserve1           : 16;

        /* Byte Segment 1 */
        uint64_t sys_tick           : 32;
        uint64_t ins_nav_stat       :  8;
        uint64_t reserve2           : 24;

        /* Byte Segment 2 */
        uint64_t ins_lat            : 64;

        /* Byte Segment 3 */
        uint64_t ins_lon            : 64;

        /* Byte Segment 4 */
        uint64_t ins_alt            : 24;
        uint64_t ins_vel_n          : 20;
        uint64_t ins_vel_e          : 20;

        /* Byte Segment 5 */
        uint64_t ins_vel_d          : 20;
        uint64_t ins_roll           : 22;
        uint64_t ins_pitch          : 22;

        /* Byte Segment 6 */
        uint64_t ins_heading        : 22;
        uint64_t reserve3           : 42;

        /* Byte Segment 7 */
        uint64_t reserve4           : 48;
        uint64_t cycle_cnt          :  8;
        uint64_t crc                :  8;
    } bit;
} InternalCan_InsFrame_TypeDef;

/* 用户标准协议，IMU数据帧结构 ImuFrame 0x0401*/
typedef union
{
    uint8_t Data[64];
    struct
    {
        /* Byte Segment 0 */
        uint64_t gps_week           : 16;
        uint64_t gps_week_second    : 32;
        uint64_t reserve5           : 16;

        /* Byte Segment 1 */
        uint64_t gyr_x              : 28;
        uint64_t gyr_y              : 28;
        uint64_t reserve6           :  8;

        /* Byte Segment 2 */
        uint64_t gyr_z              : 28;
        uint64_t acc_x              : 28;
        uint64_t reserve7          :  8;

        /* Byte Segment 3 */
        uint64_t acc_y              : 28;
        uint64_t acc_z              : 28;
        uint64_t reserve8           :  8;

        /* Byte Segment 4 */
        uint64_t gyr_temp           : 16;
        uint64_t acc_temp           : 16;
        uint64_t gyr_self_test_flg  :  2;
        uint64_t gyr_cfg_flg        :  1;
        uint64_t gyr_run_stat       :  1;
        uint64_t gyr_comu_err       :  1;
        uint64_t gyr_crc_err        :  1;
        uint64_t gyr_obt_err        :  1;
        uint64_t gyr_over_range     :  1;
        uint64_t gyr_temp_stat      :  2;
        uint64_t acc_self_test_flg  :  2;
        uint64_t acc_cfg_flg        :  1;
        uint64_t acc_run_stat       :  1;
        uint64_t acc_comu_err       :  1;
        uint64_t acc_crc_err        :  1;
        uint64_t acc_obt_err        :  1;
        uint64_t acc_over_range     :  1;
        uint64_t acc_temp_stat      :  2;
        uint64_t reserve9           : 12;

        /* Byte Segment 5 */
        uint64_t reserve10           : 64;

        /* Byte Segment 6 */
        uint64_t reserve11          : 64;

        /* Byte Segment 7 */
        uint64_t reserve12           : 48;
        uint64_t cycle_cnt          :  8;
        uint64_t crc                :  8;

    }bit;
} InternalCan_ImuFrame_TypeDef;

/* 用户标准协议，GNSS数据帧结构 GnssFrame 0x0402*/
typedef union
{
    uint8_t Data[64];
    struct
    {
        /* Byte Segment 0 */
        uint64_t gps_week           : 16;
        uint64_t gps_week_second    : 32;
        uint64_t utc_year           : 10;
        uint64_t utc_month          :  4;
        uint64_t reserve13           :  2;

        /* Byte Segment 1 */
        uint64_t utc_day            :  5;
        uint64_t utc_hour           :  5;
        uint64_t utc_min            :  6;
        uint64_t utc_sec            :  6;
        uint64_t utc_ms             : 10;
        uint64_t gnss_fix_mode      :  4;
        uint64_t gnss_head_mode     :  4;
        uint64_t gnss_diff_age      :  8;
        uint64_t gnss_sat_cnt       :  8;
        uint64_t gnss_sat_cnt_solve :  8;

        /* Byte Segment 2 */
        uint64_t gnss_lat           : 64;

        /* Byte Segment 3 */
        uint64_t gnss_lon           : 64;

        /* Byte Segment 4 */
        uint64_t gnss_alt           : 24;
        uint64_t gnss_trace_angle   : 16;
        uint64_t gnss_heading       : 16;
        uint64_t reserve14          :  8;

        /* Byte Segment 5 */
        uint64_t gnss_speed         : 16;
        uint64_t gnss_vel_n         : 16;
        uint64_t gnss_vel_e         : 16;
        uint64_t gnss_vel_d         : 16;

        /* Byte Segment 6 */
        uint64_t gnss_pdop          : 10;
        uint64_t gnss_hdop          : 10;
        uint64_t gnss_vdop          : 10;
        uint64_t gnss_lat_std       : 10;
        uint64_t gnss_lon_std       : 10;
        uint64_t gnss_head_std      : 10;
        uint64_t reserve15           :  4;

        /* Byte Segment 7 */
        uint64_t gnss_self_test_flg     :  2;
        uint64_t gnss_cfg_flg           :  1;
        uint64_t gnss_run_stat          :  1;
        uint64_t gnss_comu_stat         :  1;
        uint64_t gnss_pos_stat          :  1;
        uint64_t gnss_head_stat         :  1;
        uint64_t gnss_rtcm_stat         :  1;
        uint64_t gnss_ant1_stat         :  2;
        uint64_t gnss_ant2_stat         :  2;
        uint64_t pps_stat               :  1;
        uint64_t GpsTimeStat        : 2;
        uint64_t reserve16               : 33;
        uint64_t cycle_cnt              :  8;
        uint64_t crc                    :  8;

    }bit;
} InternalCan_GnssFrame_TypeDef;

/* 用户标准协议，状态数据帧结构 StateFrame 0x0403*/
typedef union
{
    uint8_t Data[64];
    struct
    {
        /* Byte Segment 0 */
        uint64_t gps_week           : 16;
        uint64_t gps_week_second    : 32;
        uint64_t reserve17           : 16;

        /* Byte Segment 1 */
        uint64_t sys_tick           : 32;
        uint64_t vol_sup            : 10;
        uint64_t vol_sys            : 10;
        uint64_t vol_gnss           : 10;
        uint64_t reserve18           :  2;

        /* Byte Segment 2 */
        uint64_t vol_ant            : 10;
        uint64_t sys_temp           : 16;
        uint64_t sys_work_stat      :  2;
        uint64_t core_start_up      :  1;
        uint64_t mon_start_up       :  1;
        uint64_t para_read_flag     :  1;
        uint64_t vol_sup_stat       :  2;
        uint64_t vol_sys_stat       :  2;
        uint64_t vol_gnss_stat      :  2;
        uint64_t vol_ant_stat       :  2;
        uint64_t vol_mcu_stat       :  2;
        uint64_t vol_imu_stat       :  2;
        uint64_t vol_can_stat       :  2;
        uint64_t eth_init_flg       :  2;
        uint64_t eth_dhcp_stat      :  3;
        uint64_t eth_link_stat      :  1;
        uint64_t ins_nav_stat       :  8;
        uint64_t reserve19          :  5;

        /* Byte Segment 3 */
        uint64_t gyr_stat           : 16;
        uint64_t acc_stat           : 16;
        uint64_t gnss_stat          : 16;
        uint64_t reserve20           : 16;

        /* Byte Segment 4 */
        uint64_t odom_stat          : 64;

        /* Byte Segment 5 */
        uint64_t reserve21           : 64;

        /* Byte Segment 6 */
        uint64_t reserve22           : 64;

        /* Byte Segment 7 */
        uint64_t reserve23           : 48;
        uint64_t cycle_cnt          :  8;
        uint64_t crc                :  8;
    } bit;
} InternalCan_StateFrame_TypeDef;


/* 用户标准协议，误差数据帧结构 CovFrame 0x0404*/
typedef union
{
    uint8_t Data[64];
    struct
    {
        /* Byte Segment 0 */
        uint64_t gps_week           : 16;
        uint64_t gps_week_second    : 32;
        uint64_t reserve24           : 16;

        /* Byte Segment 1 */
        uint64_t ins_lat_err        : 16;
        uint64_t ins_lon_err        : 16;
        uint64_t ins_alt_err        : 16;
        uint64_t ins_lat_conf       :  4;
        uint64_t ins_lon_conf       :  4;
        uint64_t ins_alt_conf       :  4;
        uint64_t reserve25           :  4;

        /* Byte Segment 2 */
        uint64_t ins_heading_err    : 16;
        uint64_t ins_pitch_err      : 16;
        uint64_t ins_roll_err       : 16;
        uint64_t ins_heading_conf   :  4;
        uint64_t ins_pitch_conf     :  4;
        uint64_t ins_roll_conf      :  4;
        uint64_t reserve26           :  4;

        /* Byte Segment 3 */
        uint64_t ins_vel_n_err      : 16;
        uint64_t ins_vel_e_err      : 16;
        uint64_t ins_vel_d_err      : 16;
        uint64_t ins_vel_n_conf     :  4;
        uint64_t ins_vel_e_conf     :  4;
        uint64_t ins_vel_d_conf     :  4;
        uint64_t reserve27           :  4;

        /* Byte Segment 4 */
        uint64_t reserve28          : 64;

        /* Byte Segment 5 */
        uint64_t reserve29           :  64;

        /* Byte Segment 6 */
        uint64_t reserve30           :  64;

        /* Byte Segment 7 */
        uint64_t reserve31      : 15;
        uint64_t ins_cali_prog      :  7;
        uint64_t reserve32          : 26;
        uint64_t cycle_cnt          :  8;
        uint64_t crc                :  8;
    } bit;
} InternalCan_CovFrame_TypeDef;

/* 用户标准协议，里程计数据帧结构 OdomFrame 0x0405*/
typedef union
{
    uint8_t Data[64];
    struct
    {
        /* Byte Segment 0 */
        uint64_t gps_week           : 16;
        uint64_t gps_week_second    : 32;
        uint64_t reserve33           : 16;

        /* Byte Segment 1 */
        uint64_t odom_gear          :  4;
        uint64_t wheel_spd_fl       : 20;
        uint64_t wheel_spd_fr       : 20;
        uint64_t wheel_spd_rl       : 20;

        /* Byte Segment 2 */
        uint64_t wheel_spd_rr       : 20;
        uint64_t vehicle_spd        : 20;
        uint64_t wheel_pulse_fl     : 12;
        uint64_t wheel_pulse_fr     : 12;

        /* Byte Segment 3 */
        uint64_t wheel_pulse_rl     : 12;
        uint64_t wheel_pulse_rr     : 12;
        uint64_t steer_wheel_agl    : 16;
        uint64_t steer_wheel_spd    : 16;
        uint64_t reserve34           :  8;

        /* Byte Segment 4 */
        uint64_t wheel_scale_l      : 20;
        uint64_t wheel_scale_r      : 20;
        uint64_t reserve35           : 24;

        /* Byte Segment 5 */
        uint64_t odom_run_stat      :  1;
        uint64_t gear_err           :  8;
        uint64_t wheel_spd_err      : 16;
        uint64_t veh_spd_err        :  8;
        uint64_t wheel_pulse_err    : 16;
        uint64_t steer_wheel_err    :  4;
        uint64_t reserve36           : 11;

        /* Byte Segment 6 */
        uint64_t reserve37           : 64;

        /* Byte Segment 7 */
        uint64_t reserve38           : 48;
        uint64_t cycle_cnt          :  8;
        uint64_t crc                :  8;
    }bit;
} InternalCan_OdomFrame_TypeDef;

/* VX1000 CAN标准协议 解析后需要展示数据 */
typedef struct
{
    uint16_t 	GpsWeek_ins;//		#GPS周	
    float GpsWeekSecond_ins;//	#GPS周内秒
    uint64_t 	SysTick;//		#系统工作时间
    int8_t 	NaviTick;//	#导航状态
    double InsLat;//          #融合纬度
    double InsLon;//          #融合经度
    float InsAlt;//          #融合高度
    float InsVelN;//         #融合北向速度
    float InsVelE;//         #融合东向速度
    float InsVelD;//         #融合地向速度
    float InsRoll;//         #横滚角
    float InsPitch;//        #俯仰角
    float InsHeading;//      #航向角
    uint8_t 	cycle_cnt_ins;//	#循环计数

    uint16_t 	GpsWeek_imu;//		#GPS周	
    float GpsWeekSecond_imu;//	#GPS周内秒
    double AccX;//          	#X向加速度
    double AccY;//          	#Y向加速度
    double AccZ;//          	#Z向加速度
    double GyrX;//          	#陀螺X轴角速度
    double GyrY;//          	#陀螺Y轴角速度
    double GyrZ;//          	#陀螺Z轴角速度
    float GyrTemp;//         #陀螺温度
    float AccTemp;//        #加速度计温度
    uint8_t	GyrSelfTestFlag;//	#陀螺自检标志
    bool 	GyrConfigFlag;//	#陀螺配置标志
    bool	GyrRunStat;//	#陀螺运行状态
    bool	GyrComuErr;//	#陀螺仪通讯中断异常
    bool	GyrCRCErr;//	#陀螺仪通讯CRC异常
    bool	GyrObtErr;//	#陀螺钝值异常
    bool	GyrOverRange;//	#陀螺超程告警
    uint8_t	GyrTempStat;//	#陀螺温度状态
    bool 	AccSelfTestFlag;//	#加表自检标志
    bool	AccConfigFlag;//	#加表配置标志
    bool	AccRunStat;//	#加表运行状态
    bool	AccComuErr;//	#加表通讯中断异常
    bool	AccCRCErr;//	#加表通讯CRC异常
    bool	AccObtErr;//	#加表钝值异常
    bool	AccOverRange;//	#加表超程告警
    uint8_t	AccTempStat;//	#加表温度状态
    uint8_t 	cycle_cnt_imu;//	#循环计数

    uint16_t 	GpsWeek_gnss;//		#GPS周	
    float GpsWeekSecond_gnss;//	#GPS周内秒
    uint16_t  UtcYear;//		#UTC时间年
    uint16_t  UtcMonth;//	#UTC时间月
    uint16_t  UtcDay;//		#UTC时间日
    uint16_t  UtcHour;//		#UTC时间小时
    uint16_t  UtcMinute;//	#UTC时间分钟
    uint16_t  UtcSecond;//	#UTC时间秒
    uint16_t  UtcMillisecond;//	#UTC时间毫秒
    uint8_t 	GnssFixMode;//     #定位模式
    uint8_t	GnssHeadMode;//	#双天线航向模式
    uint16_t 	GnssDiffAge;//     #差分龄期
    uint8_t 	GnssSatCnt;//      #观测卫星数量
    uint8_t 	GnssSatCntSlove;// #解算卫星数量
    double GnssLat;//         #GNSS纬度
    double GnssLon;//         #GNSS经度
    float GnssAlt;//         #GNSS高度
    float GnssTraceAngle;//  #GNSS航迹航向角
    float GnssHeading;//  	#GNSS双天线航向角
    float GnssSpeed;//	#GNSS水平速度
    float GnssVelN;//        #GNSS北向速度
    float GnssVelE;//        #GNSS东向速度
    float GnssVelD;//        #GNSS地向速度
    float GnssPDOP;//        #位置精度因子
    float GnssHDOP;//        #水平精度因子
    float GnssVDOP;//        #垂直精度因子
    float GnssLatStd;//      #纬度标准差
    float GnssLonStd;//      #经度标准差
    float GnssHeadStd;//     #双天线航向标准差
    bool	GnssSelfTestFlag;// #GNSS模块自检标志
    bool	GnssConfigFlag;//	#GNSS模块配置标志
    bool 	GnssRunStat;//     #GNSS模块运行状态
    bool	GnssComuStat;//	#GNSS通讯接口状态
    bool	GnssPosStat;//	#GNSS定位状态
    bool	GnssHeadStat;//	#GNSS定向状态
    bool	GnssRtcmStat;//	#GNSS差分状态
    uint8_t	GnssAnt1Stat;//	#GNSS主天线状态
    uint8_t	GnssAnt2Stat;//	#GNSS副天线状态
    bool 	PPSStat;//	   	#秒脉冲信号状态
    uint8_t 	GpsTimeStat;//    	#GPS时间状态
    uint8_t 	cycle_cnt_gnss;//	#循环计数

    uint16_t 	GpsWeek_stat;//		#GPS周	
    float GpsWeekSecond_stat;//	#GPS周内秒
    uint32_t SysTick_stat;// #上电工作时间
    float	VolSup;//		#设备供电电压
    float	VolSys;//		#系统供电电压
    float	VolGnss;//		#GNSS模块供电电压
    float	VolAnt;//		#GNSS天线供电电压
    float	SysTemp;//		#系统温度
    uint8_t	SysWorkStat;//	#系统工作状态
    bool	CoreStartUp;//	#内核启动标志
    bool	MonStartUp;//	#监控单元启动标志
    bool	ParaReadFlag;//	#设备参数加载标志
    uint8_t	VolSupStat;//	#设备供电电压状态
    uint8_t	VolSysStat;//	#系统供电电压状态
    uint8_t	VolGnssStat;//	#GNSS模块电压状态
    uint8_t	VolAntStat;//	#GNSS天线电压状态
    uint8_t	VolMcuStat;//	#MCU电压状态
    uint8_t	VolImuStat;//	#IMU电压状态
    uint8_t	VolCanStat;//	#CAN电压状态
    uint8_t	EthInitFlag;//	#车载以太网初始化状态
    uint8_t	EthDHCPStat;//	#车载以太网自动分配IP状态
    bool	EthLinkStat;//	#车载以太网连接状态
    int8_t	InsNavStat;//	#导航状态
    uint16_t GyrStat_stat; //#陀螺仪状态
    uint16_t AccStat_stat; // #加速度计状态
    uint16_t GnssStat_stat; // #GNSS模块状态
    uint64_t OdomStat_stat; // #里程计状态
    uint8_t 	cycle_cnt_state;// #循环计数

    uint16_t 	GpsWeek_cov;//		#GPS周	
    float GpsWeekSecond_cov;//	#GPS周内秒
    float	InsLatErr;//	#纬度误差
    float	InsLonErr;//	#经度误差
    float InsAltErr;//	#高度误差
    uint8_t	InsLatConf;//	#纬度置信度
    uint8_t	InsLonConf;//	#经度置信度
    uint8_t	InsAltConf;//	#高度置信度
    float	InsHeadingErr;//	#航向角误差
    float	InsPitchErr;//	#俯仰角误差
    float	InsRollErr;//	#横滚角误差
    uint8_t	InsHeadingConf;//	#航向角置信度
    uint8_t	InsPitchConf;//	#俯仰角置信度
    uint8_t	InsRollConf;//	#横滚角置信度
    float	InsVelNErr;//	#北向速度误差
    float	InsVelEErr;//	#东向速度误差
    float	InsVelDErr;//	#地向速度误差
    uint8_t	InsVelNConf;//	#北向速度置信度
    uint8_t	InsVelEConf;//	#东向速度置信度
    uint8_t	InsVelDConf;//	#地向速度置信度
    uint8_t 	InsCaliProg;//     #标定进度
    uint8_t 	cycle_cnt_cov;//	#循环计数

    uint16_t 	GpsWeek_odom;//		#GPS周	
    float GpsWeekSecond_odom;//	#GPS周内秒
    uint8_t 	OdomGear;//        #档位
    float WheelSpdFl;//      #左前轮速度
    float WheelSpdFr;//      #右前轮速度
    float WheelSpdRl;//      #左后轮速度
    float WheelSpdRr;//      #右后轮速度
    float VehicleSpd;//      #整车速度
    uint16_t	WheelPulseFl;//	#左前轮脉冲
    uint16_t	WheelPulseFr;//	#右前轮脉冲
    uint16_t	WheelPulseRl;//	#左后轮脉冲
    uint16_t	WheelPulseRr;//	#右后轮脉冲
    float	SteerWheelAngle;//	#方向盘角度
    uint16_t	SteerWheelSpeed;//	#方向盘转动速度
    float	WheelScaleL;//	#左轮速度刻度因数
    float	WheelScaleR;//	#右轮速度刻度因数
    bool 	OdomRunStat;//     #里程计运行状态
    uint8_t	GearErr;//		#档位数据异常
    uint16_t	WheelSpdErr;//	#四轮速异常
    uint8_t	VehicleSpdErr;//	#整车速异常
    uint16_t	WheelPulseErr;//	#四轮脉冲异常
    uint8_t	SteerWheelErr;//	#方向盘数据异常
    uint8_t 	cycle_cnt_odom;//	#循环计数

}VxNaviUserData_TypeDef;
#pragma pack()

class VX1000_Drv
{
private:
    ros::NodeHandle &node_handle;
    Rec_Queue &queue;
    bool match = true;
    ros::Publisher vx1000_ins_pub;
    ros::Publisher vx1000_imu_pub;
    ros::Publisher vx1000_gnss_pub;
    ros::Publisher vx1000_stat_pub;
    ros::Publisher vx1000_cov_pub;
    ros::Publisher vx1000_odom_pub;

    InternalCan_InsFrame_TypeDef m_InsFrame_Typ;
    InternalCan_ImuFrame_TypeDef m_ImuFrame_Typ;
    InternalCan_GnssFrame_TypeDef m_GnssFrame_Typ;
    InternalCan_StateFrame_TypeDef m_StateFrame_Typ;
    InternalCan_CovFrame_TypeDef m_CovFrame_Typ;
    InternalCan_OdomFrame_TypeDef m_OdomFrame_Typ;

    void Decode();

public:
    VX1000_Drv(ros::NodeHandle &nh,Rec_Queue &q);
    ~VX1000_Drv();

    int Proto_INSP_UnPack_Vx1000_RxChar(uint8_t* buf);
    void Parase();
};




#endif
