FROM osrf/ros:noetic-desktop-full   
# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
${NVIDIA_VISIBLE_DEVICES:-all}
 
ENV NVIDIA_DRIVER_CAPABILITIES \
${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics
 
# 设置工作路径
WORKDIR /home

# 拷贝文件
COPY /catkin_ws /home/

#设置时区为上海
ENV TZ=Asia/Shanghai

 
RUN apt-get update && \
apt-get install -y \
build-essential \
libgl1-mesa-dev \
libglew-dev \
libsdl2-dev \
libsdl2-image-dev \
libglm-dev \
libfreetype6-dev \
libglfw3-dev \
libglfw3 \
libglu1-mesa-dev \
freeglut3-dev \
vim \
git \
libpcl-dev \
libcanberra-gtk-module
 


