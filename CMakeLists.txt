cmake_minimum_required(VERSION 2.8.3)
add_definitions(-std=c++11)

project(vx1000_drv)


set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g ")
set (CMAKE_VERBOSE_MAKEFILE ON)


# # 设置要拷贝的文件夹的源路径和目标路径
# set(SOURCE_FOLDER_PATH ${PROJECT_SOURCE_DIR}/src/js)
# set(TARGET_FOLDER_PATH ${CMAKE_BINARY_DIR}/js)

add_definitions(-DBOOST_LOG_DYN_LINK)

# 此处仅添加thread和chrono  你可以添加你需要的
find_package(Boost REQUIRED COMPONENTS
        locale thread log filesystem chrono container context
)
if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIR})
endif()


## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  geometry_msgs
  sensor_msgs
  serial
  std_msgs
  std_srvs
  tf
  system_msgs
)

# Find mqtt client
find_package(PahoMqttCpp REQUIRED)



# set(JS_FILES
#     ./js/map_load.js
#     ./js/convert.js
#     ./js/bmap_offline_api_v3.0_min.js
#     ./js/map_plus.js
#     # 添加更多的 .js 文件
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
#  LIBRARIES imu
  CATKIN_DEPENDS roscpp geometry_msgs sensor_msgs serial std_msgs std_srvs tf
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  
  ./src
)

# 包含QuickJS头文件的路径
include_directories(/usr/local/include/quickjs)
## Declare a C++ executable
add_executable(vx1000_drv src/VX1000_Drv.cpp
                             src/main.cpp
                             src/Rec_Thread.cpp
                             src/Rec_Queue.cpp
                             src/mqtt_test.cpp
                             src/Common.cpp
                             src/seed_log_api.cpp
                             src/seed_log_local.cpp
                             src/seed_log_example.cpp
                             src/Robot.cpp
                             src/Vcu_Thread.cpp
                             )



## Specify libraries to link a library or executable target against
target_link_libraries(vx1000_drv
  ${catkin_LIBRARIES}
)
# 连接boost库
target_link_libraries(${PROJECT_NAME} ${Boost_LOG_LIBRARY})

target_link_libraries(${PROJECT_NAME}
  PahoMqttCpp::paho-mqttpp3
)
# 链接QuickJS库






#############
## Install ##
#############

# 添加一个自定义命令来拷贝文件夹
# add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
#   COMMAND ${CMAKE_COMMAND} -E make_directory $<TARGET_FILE_DIR:${PROJECT_NAME}>/js
#   COMMAND ${CMAKE_COMMAND} -E copy_directory ${SOURCE_FOLDER_PATH} $<TARGET_FILE_DIR:${PROJECT_NAME}>/js
# )

## Mark executables and/or libraries for installation
# install(TARGETS base_imu_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

# install (DIRECTORY launch
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

# install (DIRECTORY rviz
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

# add_executable(vx1000_sub src/node_subscriber.cpp)
# #add_executable(vx1000_pub src/VX1000_Drv.cpp)

# target_link_libraries(vx1000_sub ${catkin_LIBRARIES})
# target_link_libraries(vx1000_drv ${catkin_LIBRARIES})

