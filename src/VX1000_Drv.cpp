#include "VX1000_Drv.h"
#include <fstream>
#include <iostream>
#include <string>


using namespace std;

system_msgs::vx1000_ins vx1000_ins_data;
system_msgs::vx1000_imu vx1000_imu_data;
system_msgs::vx1000_gnss vx1000_gnss_data;
system_msgs::vx1000_stat vx1000_stat_data;
system_msgs::vx1000_cov vx1000_cov_data;
system_msgs::vx1000_odom vx1000_odom_data;
string data_save;

VX1000_Drv::VX1000_Drv(ros::NodeHandle &nh,Rec_Queue &q):node_handle(nh),queue(q)
{
    this->vx1000_ins_pub = node_handle.advertise<system_msgs::vx1000_ins>("VX1000_ins", 1024);
    this->vx1000_imu_pub = node_handle.advertise<system_msgs::vx1000_imu>("VX1000_imu", 1024);
    this->vx1000_gnss_pub = node_handle.advertise<system_msgs::vx1000_gnss>("VX1000_gnss", 1024);
    this->vx1000_stat_pub = node_handle.advertise<system_msgs::vx1000_stat>("VX1000_stat", 1024);
    this->vx1000_cov_pub = node_handle.advertise<system_msgs::vx1000_cov>("VX1000_cov", 1024);
    this->vx1000_odom_pub = node_handle.advertise<system_msgs::vx1000_odom>("VX1000_odom", 1024);
}

VX1000_Drv::~VX1000_Drv()
{

}

void VX1000_Drv::Decode()
{
    uint8_t msg[1] = {0};

    uint8_t vx1000_buf_temp[Vx1000_PROTO_LEN*5] = {0};
    uint8_t vx1000_buf[5][Vx1000_PROTO_LEN] = {0};
	

    if(this->queue.GetSize() < sizeof(vx1000_buf_temp))
        return;
    
    this->queue.Pop(vx1000_buf_temp, sizeof(vx1000_buf_temp));
     
    uint32_t k = 0;
    uint8_t vx1000_size = sizeof(vx1000_buf_temp)/Vx1000_PROTO_LEN;

    for(uint8_t i = 0; i < vx1000_size; i++)
    {
        for(uint8_t j = 0;j<Vx1000_PROTO_LEN;j++)
        {
            vx1000_buf[i][j] = vx1000_buf_temp[k++]; 
        }
    }

    for(uint8_t i = 0 ; i< (sizeof(vx1000_buf_temp)/Vx1000_PROTO_LEN) ;i++ )
    {
        Proto_INSP_UnPack_Vx1000_RxChar(vx1000_buf[i]);
    }
}

void VX1000_Drv::Parase()
{
    uint8_t mark[2] = {0};
    uint8_t msg[2] = {0};
    uint8_t *buff = NULL;

    if((this->queue.GetSize() >= Vx1000_PROTO_LEN*5) && ((this->queue.GetState() == RecQueue_ok) || (this->queue.GetState() == RecQueue_full)))
    {
        if(this->match)
        {
            for(uint32_t index = 0; index < this->queue.GetSize(); index ++)
            {
               this->queue.Check(index, mark, sizeof(mark));

                if((mark[0] == VX1000_FRAME_HEAD_0) && (mark[1] == VX1000_FRAME_HEAD_1))
                {
                    this->match = false;
                    buff = (uint8_t *)malloc(index);
                    this->queue.Pop(buff, index);
                    free(buff);

                    printf("VX1000 Frame Align match pos : %d\r\n", index);
                    break;
                }
                else
                {
                    uint8_t  data_delete[2] = {0};
                    this->queue.Pop(data_delete, 1);
                    this->queue.Check(0, msg, sizeof(msg));
                }
            }
        }
        else
        {
            this->queue.Check(0, msg, sizeof(msg));
            if((msg[0] == VX1000_FRAME_HEAD_0) && 
                (msg[1] == VX1000_FRAME_HEAD_1))
            {
                Decode();
            }
            else
            {
                uint8_t  data_delete[2] = {0};
                this->queue.Pop(data_delete, 1);
            }
        }
    }
}

int VX1000_Drv::Proto_INSP_UnPack_Vx1000_RxChar(uint8_t* buf)
{
    uint8_t check_sum = 0;
    for(uint8_t j = 0; j < Vx1000_PROTO_LEN - 1; j++)
    {
        check_sum += buf[j];
    }

    if(check_sum == buf[Vx1000_PROTO_LEN-1])
    {
        switch (buf[2])
        {
            case VX1000_TYPE_1:
                    {
                        memcpy(&m_InsFrame_Typ.Data,&buf[3],sizeof (InternalCan_InsFrame_TypeDef));

                        vx1000_ins_data.GpsWeek =  m_InsFrame_Typ.bit.gps_week;
                        vx1000_ins_data.GpsWeekSecond =  m_InsFrame_Typ.bit.gps_week_second * 1e-3;
                        vx1000_ins_data.SysTick =  m_InsFrame_Typ.bit.sys_tick;
                        vx1000_ins_data.NaviTick =  m_InsFrame_Typ.bit.ins_nav_stat - 1;
                        vx1000_ins_data.InsLat =  m_InsFrame_Typ.bit.ins_lat * 1e-10 - 90;
                        vx1000_ins_data.InsLon =  m_InsFrame_Typ.bit.ins_lon * 1e-10 - 180;
                        vx1000_ins_data.InsAlt =  m_InsFrame_Typ.bit.ins_alt * 1e-3 -1000;
                        vx1000_ins_data.InsVelN =  m_InsFrame_Typ.bit.ins_vel_n * 1e-3 - 100;
                        vx1000_ins_data.InsVelE =  m_InsFrame_Typ.bit.ins_vel_e * 1e-3 - 100;
                        vx1000_ins_data.InsVelD =  m_InsFrame_Typ.bit.ins_vel_d * 1e-3 - 100;
                        vx1000_ins_data.InsRoll =  m_InsFrame_Typ.bit.ins_roll * 1e-4 -180;
                        vx1000_ins_data.InsPitch =  m_InsFrame_Typ.bit.ins_pitch *1e-4 -90;
                        vx1000_ins_data.InsHeading =  m_InsFrame_Typ.bit.ins_heading * 1e-4;
                        vx1000_ins_data.cycle_cnt_ins =  m_InsFrame_Typ.bit.cycle_cnt;

                        this->vx1000_ins_pub.publish(vx1000_ins_data);
                    }
                break;
            case VX1000_TYPE_2:
                    {
                        memcpy(&m_ImuFrame_Typ.Data,&buf[3],sizeof (InternalCan_ImuFrame_TypeDef));

                        vx1000_imu_data.GpsWeek =  m_ImuFrame_Typ.bit.gps_week;
                        vx1000_imu_data.GpsWeekSecond =  m_ImuFrame_Typ.bit.gps_week_second * 1e-3;
                        vx1000_imu_data.GyrX =   m_ImuFrame_Typ.bit.gyr_x * 1e-5 - 500;
                        vx1000_imu_data.GyrY =   m_ImuFrame_Typ.bit.gyr_y * 1e-5 - 500;  
                        vx1000_imu_data.GyrZ =   m_ImuFrame_Typ.bit.gyr_z * 1e-5 - 500;  
                        vx1000_imu_data.AccX =   m_ImuFrame_Typ.bit.acc_x * 1e-5 - 400;  
                        vx1000_imu_data.AccY =   m_ImuFrame_Typ.bit.acc_y * 1e-5 - 400;  
                        vx1000_imu_data.AccZ =   m_ImuFrame_Typ.bit.acc_z * 1e-5 - 400;  
                        vx1000_imu_data.GyrTemp =   m_ImuFrame_Typ.bit.gyr_temp * 1e-2 - 128;  
                        vx1000_imu_data.AccTemp =   m_ImuFrame_Typ.bit.acc_temp * 1e-2 - 128;  
                        vx1000_imu_data.GyrSelfTestFlag = m_ImuFrame_Typ.bit.gyr_self_test_flg;
                        vx1000_imu_data.GyrConfigFlag = m_ImuFrame_Typ.bit.gyr_cfg_flg;
                        vx1000_imu_data.GyrRunStat =   m_ImuFrame_Typ.bit.gyr_run_stat;  
                        vx1000_imu_data.GyrComuErr = m_ImuFrame_Typ.bit.gyr_comu_err;
                        vx1000_imu_data.GyrCRCErr = m_ImuFrame_Typ.bit.gyr_crc_err;
                        vx1000_imu_data.GyrObtErr = m_ImuFrame_Typ.bit.gyr_obt_err;
                        vx1000_imu_data.GyrOverRange = m_ImuFrame_Typ.bit.gyr_over_range;
                        vx1000_imu_data.GyrTempStat = m_ImuFrame_Typ.bit.gyr_temp_stat;
                        vx1000_imu_data.AccSelfTestFlag = m_ImuFrame_Typ.bit.acc_self_test_flg;
                        vx1000_imu_data.AccConfigFlag = m_ImuFrame_Typ.bit.acc_cfg_flg;
                        vx1000_imu_data.AccRunStat =   m_ImuFrame_Typ.bit.acc_run_stat;
                        vx1000_imu_data.AccComuErr = m_ImuFrame_Typ.bit.acc_comu_err;
                        vx1000_imu_data.AccCRCErr = m_ImuFrame_Typ.bit.acc_crc_err;
                        vx1000_imu_data.AccObtErr = m_ImuFrame_Typ.bit.acc_obt_err;
                        vx1000_imu_data.AccOverRange = m_ImuFrame_Typ.bit.acc_over_range;
                        vx1000_imu_data.AccTempStat = m_ImuFrame_Typ.bit.acc_temp_stat;
                        vx1000_imu_data.cycle_cnt_imu =   m_ImuFrame_Typ.bit.cycle_cnt;      

                        this->vx1000_imu_pub.publish(vx1000_imu_data);        

                    }
                break;
            case VX1000_TYPE_3:
                    {
                        memcpy(&m_GnssFrame_Typ.Data,&buf[3],sizeof (InternalCan_GnssFrame_TypeDef));

                        vx1000_gnss_data.GpsWeek =  m_GnssFrame_Typ.bit.gps_week;
                        vx1000_gnss_data.GpsWeekSecond =  m_GnssFrame_Typ.bit.gps_week_second * 1e-3;
                        vx1000_gnss_data.UtcYear = m_GnssFrame_Typ.bit.utc_year - 2000;
                        vx1000_gnss_data.UtcMonth = m_GnssFrame_Typ.bit.utc_month;
                        vx1000_gnss_data.UtcDay = m_GnssFrame_Typ.bit.utc_day;
                        vx1000_gnss_data.UtcHour = m_GnssFrame_Typ.bit.utc_hour;
                        vx1000_gnss_data.UtcMinute = m_GnssFrame_Typ.bit.utc_min;
                        vx1000_gnss_data.UtcSecond = m_GnssFrame_Typ.bit.utc_sec;
                        vx1000_gnss_data.UtcMillisecond = m_GnssFrame_Typ.bit.utc_ms;
                        vx1000_gnss_data.GnssFixMode = m_GnssFrame_Typ.bit.gnss_fix_mode;
                        vx1000_gnss_data.GnssHeadMode = m_GnssFrame_Typ.bit.gnss_head_mode;
                        vx1000_gnss_data.GnssDiffAge = m_GnssFrame_Typ.bit.gnss_diff_age;
                        vx1000_gnss_data.GnssSatCnt = m_GnssFrame_Typ.bit.gnss_sat_cnt;
                        vx1000_gnss_data.GnssSatCntSlove = m_GnssFrame_Typ.bit.gnss_sat_cnt_solve;
                        vx1000_gnss_data.GnssLat  =   m_GnssFrame_Typ.bit.gnss_lat * 1e-10 -90;       
                        vx1000_gnss_data.GnssLon  =   m_GnssFrame_Typ.bit.gnss_lon * 1e-10 -180;      
                        vx1000_gnss_data.GnssAlt  =   m_GnssFrame_Typ.bit.gnss_alt * 1e-3 -1000;        
                        vx1000_gnss_data.GnssTraceAngle  =   m_GnssFrame_Typ.bit.gnss_trace_angle * 1e-2; 
                        vx1000_gnss_data.GnssHeading = m_GnssFrame_Typ.bit.gnss_heading * 1e-2; 
                        vx1000_gnss_data.GnssSpeed = m_GnssFrame_Typ.bit.gnss_speed * 1e-2;
                        vx1000_gnss_data.GnssVelN  =   m_GnssFrame_Typ.bit.gnss_vel_n *1e-2 - 100;  
                        vx1000_gnss_data.GnssVelE  =   m_GnssFrame_Typ.bit.gnss_vel_e *1e-2 - 100;   
                        vx1000_gnss_data.GnssVelD  =   m_GnssFrame_Typ.bit.gnss_vel_d *1e-2 - 100;      
                        vx1000_gnss_data.GnssPDOP  =   m_GnssFrame_Typ.bit.gnss_pdop * 1e-2;
                        vx1000_gnss_data.GnssHDOP  =   m_GnssFrame_Typ.bit.gnss_hdop * 1e-2;    
                        vx1000_gnss_data.GnssVDOP  =   m_GnssFrame_Typ.bit.gnss_vdop * 1e-2;    
                        vx1000_gnss_data.GnssLatStd  =   m_GnssFrame_Typ.bit.gnss_lat_std * 1e-2;   
                        vx1000_gnss_data.GnssLonStd  =   m_GnssFrame_Typ.bit.gnss_lon_std * 1e-2;  
                        vx1000_gnss_data.GnssHeadStd  =   m_GnssFrame_Typ.bit.gnss_head_std * 1e-2;
                        vx1000_gnss_data.GnssSelfTestFlag = m_GnssFrame_Typ.bit.gnss_self_test_flg;
                        vx1000_gnss_data.GnssConfigFlag = m_GnssFrame_Typ.bit.gnss_cfg_flg;
                        vx1000_gnss_data.GnssRunStat  =   m_GnssFrame_Typ.bit.gnss_run_stat;
                        vx1000_gnss_data.GnssComuStat = m_GnssFrame_Typ.bit.gnss_comu_stat;
                        vx1000_gnss_data.GnssPosStat = m_GnssFrame_Typ.bit.gnss_pos_stat;
                        vx1000_gnss_data.GnssHeadStat = m_GnssFrame_Typ.bit.gnss_head_stat;
                        vx1000_gnss_data.GnssRtcmStat = m_GnssFrame_Typ.bit.gnss_rtcm_stat;
                        vx1000_gnss_data.GnssAnt1Stat = m_GnssFrame_Typ.bit.gnss_ant1_stat;
                        vx1000_gnss_data.GnssAnt2Stat = m_GnssFrame_Typ.bit.gnss_ant2_stat;
                        vx1000_gnss_data.PPSStat = m_GnssFrame_Typ.bit.pps_stat;
                        vx1000_gnss_data.GpsTimeStat = m_GnssFrame_Typ.bit.GpsTimeStat;
                        vx1000_gnss_data.cycle_cnt_gnss = m_GnssFrame_Typ.bit.cycle_cnt;     

                        this->vx1000_gnss_pub.publish(vx1000_gnss_data);

                    }
                break;
            case VX1000_TYPE_4:
                    {
                        memcpy(&m_StateFrame_Typ.Data,&buf[3],sizeof (InternalCan_StateFrame_TypeDef));

                        vx1000_stat_data.GpsWeek =  m_StateFrame_Typ.bit.gps_week;
                        vx1000_stat_data.GpsWeekSecond =  m_StateFrame_Typ.bit.gps_week_second * 1e-3;
                        vx1000_stat_data.SysTick = m_StateFrame_Typ.bit.sys_tick;
                        vx1000_stat_data.VolSup = m_StateFrame_Typ.bit.vol_sup * 1e-1 - 40;
                        vx1000_stat_data.VolSys = m_StateFrame_Typ.bit.vol_sys * 1e-1 - 40;
                        vx1000_stat_data.VolGnss = m_StateFrame_Typ.bit.vol_gnss * 1e-1 - 40;
                        vx1000_stat_data.VolAnt = m_StateFrame_Typ.bit.vol_ant * 1e-1 - 40;
                        vx1000_stat_data.SysTemp = m_StateFrame_Typ.bit.sys_temp * 1e-2 - 128;
                        vx1000_stat_data.SysWorkStat = m_StateFrame_Typ.bit.sys_work_stat;
                        vx1000_stat_data.CoreStartUp = m_StateFrame_Typ.bit.core_start_up;
                        vx1000_stat_data.MonStartUp = m_StateFrame_Typ.bit.mon_start_up;
                        vx1000_stat_data.ParaReadFlag = m_StateFrame_Typ.bit.para_read_flag;
                        vx1000_stat_data.VolSupStat = m_StateFrame_Typ.bit.vol_sup_stat;
                        vx1000_stat_data.VolSysStat = m_StateFrame_Typ.bit.vol_sys_stat;
                        vx1000_stat_data.VolGnssStat = m_StateFrame_Typ.bit.vol_gnss_stat;
                        vx1000_stat_data.VolAntStat = m_StateFrame_Typ.bit.vol_ant_stat;
                        vx1000_stat_data.VolMcuStat = m_StateFrame_Typ.bit.vol_mcu_stat;
                        vx1000_stat_data.VolImuStat = m_StateFrame_Typ.bit.vol_imu_stat;
                        vx1000_stat_data.VolCanStat = m_StateFrame_Typ.bit.vol_can_stat;
                        vx1000_stat_data.EthInitFlag = m_StateFrame_Typ.bit.eth_init_flg;
                        vx1000_stat_data.EthDHCPStat = m_StateFrame_Typ.bit.eth_dhcp_stat;
                        vx1000_stat_data.EthLinkStat = m_StateFrame_Typ.bit.eth_link_stat;
                        vx1000_stat_data.InsNavStat = m_StateFrame_Typ.bit.ins_nav_stat - 1;
                        vx1000_stat_data.GyrStat = m_StateFrame_Typ.bit.gyr_stat;
                        vx1000_stat_data.AccStat = m_StateFrame_Typ.bit.acc_stat;
                        vx1000_stat_data.GnssStat = m_StateFrame_Typ.bit.gnss_stat;
                        vx1000_stat_data.OdomStat = m_StateFrame_Typ.bit.odom_stat;
                        vx1000_stat_data.cycle_cnt_state = m_StateFrame_Typ.bit.cycle_cnt;

                        this->vx1000_stat_pub.publish(vx1000_stat_data);
                    }
                break;
            case VX1000_TYPE_5:
                    {
                        memcpy(&m_CovFrame_Typ.Data,&buf[3],sizeof (InternalCan_CovFrame_TypeDef));

                        vx1000_cov_data.GpsWeek =  m_CovFrame_Typ.bit.gps_week;
                        vx1000_cov_data.GpsWeekSecond =  m_CovFrame_Typ.bit.gps_week_second * 1e-3;
                        vx1000_cov_data.InsLatErr = m_CovFrame_Typ.bit.ins_lat_err * 1e-3;
                        vx1000_cov_data.InsLonErr = m_CovFrame_Typ.bit.ins_lon_err * 1e-3;
                        vx1000_cov_data.InsAltErr = m_CovFrame_Typ.bit.ins_alt_err * 1e-3;
                        vx1000_cov_data.InsLatConf = m_CovFrame_Typ.bit.ins_lat_conf;
                        vx1000_cov_data.InsLonConf = m_CovFrame_Typ.bit.ins_lon_conf;
                        vx1000_cov_data.InsAltConf = m_CovFrame_Typ.bit.ins_alt_conf;
                        vx1000_cov_data.InsHeadingErr = m_CovFrame_Typ.bit.ins_heading_err * 1e-3;
                        vx1000_cov_data.InsPitchErr = m_CovFrame_Typ.bit.ins_pitch_err * 1e-3;
                        vx1000_cov_data.InsRollErr = m_CovFrame_Typ.bit.ins_roll_err * 1e-3;
                        vx1000_cov_data.InsHeadingConf = m_CovFrame_Typ.bit.ins_heading_conf;
                        vx1000_cov_data.InsPitchConf = m_CovFrame_Typ.bit.ins_pitch_conf;
                        vx1000_cov_data.InsRollConf = m_CovFrame_Typ.bit.ins_roll_conf;
                        vx1000_cov_data.InsVelNErr = m_CovFrame_Typ.bit.ins_vel_n_err * 1e-3;
                        vx1000_cov_data.InsVelEErr = m_CovFrame_Typ.bit.ins_vel_e_err * 1e-3;
                        vx1000_cov_data.InsVelDErr = m_CovFrame_Typ.bit.ins_vel_d_err * 1e-3;
                        vx1000_cov_data.InsVelNConf = m_CovFrame_Typ.bit.ins_vel_n_conf;
                        vx1000_cov_data.InsVelEConf = m_CovFrame_Typ.bit.ins_vel_e_conf;
                        vx1000_cov_data.InsVelDConf = m_CovFrame_Typ.bit.ins_vel_d_conf;
                        vx1000_cov_data.InsCaliProg = m_CovFrame_Typ.bit.ins_cali_prog;
                        vx1000_cov_data.cycle_cnt_cov = m_CovFrame_Typ.bit.cycle_cnt;

                        this->vx1000_cov_pub.publish(vx1000_cov_data);
                    }
                break;
            case VX1000_TYPE_6:
                    {
                        memcpy(&m_OdomFrame_Typ.Data,&buf[3],sizeof (InternalCan_OdomFrame_TypeDef));

                        vx1000_odom_data.GpsWeek =  m_OdomFrame_Typ.bit.gps_week;
                        vx1000_odom_data.GpsWeekSecond =  m_OdomFrame_Typ.bit.gps_week_second * 1e-3;
                        vx1000_odom_data.OdomGear = m_OdomFrame_Typ.bit.odom_gear;
                        vx1000_odom_data.WheelSpdFl = m_OdomFrame_Typ.bit.wheel_spd_fl * 1e-3 - 100;
                        vx1000_odom_data.WheelSpdFr = m_OdomFrame_Typ.bit.wheel_spd_fr * 1e-3 - 100;
                        vx1000_odom_data.WheelSpdRl = m_OdomFrame_Typ.bit.wheel_spd_rl * 1e-3 - 100;
                        vx1000_odom_data.WheelSpdRr = m_OdomFrame_Typ.bit.wheel_spd_rr * 1e-3 - 100;
                        vx1000_odom_data.VehicleSpd = m_OdomFrame_Typ.bit.vehicle_spd * 1e-3 - 100;
                        vx1000_odom_data.WheelPulseFl = m_OdomFrame_Typ.bit.wheel_pulse_fl;
                        vx1000_odom_data.WheelPulseFr = m_OdomFrame_Typ.bit.wheel_pulse_fr;
                        vx1000_odom_data.WheelPulseRl = m_OdomFrame_Typ.bit.wheel_pulse_rl;
                        vx1000_odom_data.WheelPulseRr = m_OdomFrame_Typ.bit.wheel_pulse_rr;
                        vx1000_odom_data.SteerWheelAngle = m_OdomFrame_Typ.bit.steer_wheel_agl * 1e-1 - 720;
                        vx1000_odom_data.SteerWheelSpeed = m_OdomFrame_Typ.bit.steer_wheel_spd * 2;
                        vx1000_odom_data.WheelScaleL = m_OdomFrame_Typ.bit.wheel_scale_l * 1e-5 - 5;
                        vx1000_odom_data.WheelScaleR = m_OdomFrame_Typ.bit.wheel_scale_r * 1e-5 - 5;
                        vx1000_odom_data.OdomRunStat = m_OdomFrame_Typ.bit.odom_run_stat;
                        vx1000_odom_data.GearErr = m_OdomFrame_Typ.bit.gear_err;
                        vx1000_odom_data.WheelSpdErr = m_OdomFrame_Typ.bit.wheel_spd_err;
                        vx1000_odom_data.VehicleSpdErr = m_OdomFrame_Typ.bit.veh_spd_err;
                        vx1000_odom_data.WheelPulseErr = m_OdomFrame_Typ.bit.wheel_pulse_err;
                        vx1000_odom_data.SteerWheelErr = m_OdomFrame_Typ.bit.steer_wheel_err;
                        vx1000_odom_data.cycle_cnt_odom = m_OdomFrame_Typ.bit.cycle_cnt;

                        this->vx1000_odom_pub.publish(vx1000_odom_data);
                    }
                break;
            }
        }
        else
        {
        }
    return 0;
}
