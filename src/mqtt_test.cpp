
#include "mqtt_test.h"
using namespace aigc;

mqtt_test::mqtt_test(/* args */)
{
    
}

mqtt_test::~mqtt_test()
{
}

Mqtt_BasePackage mqtt_test::mqtt_base_test()
{
    string basedata=R"({
 		"msgType":"robot",
 		"imei":"D999SHDXJH000SHK",
 		"type":1001,
 		"timeStamp":1673484145
 })";
    Mqtt_BasePackage mqtt_base;
	JsonHelper::JsonToObject(mqtt_base, basedata);
    return mqtt_base;
}

Mqtt_Task mqtt_test::mqtt_task_test()
{
    string data=R"({
 "msgType":"robot",
 "imei":"D999SHDXJH000SHK",
 "type":1001,
 "timeStamp":1673484145,
 "data":{
 "pathID": 1,
 "waypoint": "0 118.6542856 36.2587463,1 118.6542856 36.2587463,2 118.6542856 36.2587463,3 118.6542856 36.2587463,4 118.6542856 36.2587463"
 }
 })";
    Mqtt_Task mqtt_task ;
	JsonHelper::JsonToObject(mqtt_task, data);
    
    return mqtt_task;
}

Mqtt_Report mqtt_test::mqtt_report_test()
{
    string data=R"({
	"msgType": "robot",
	"imei": "D999SHDXJH000SHK",
	"type": 2002,
	"timeStamp": 1673484145,
	"data": {
		"vSpeed": 0.8,
		"rSpeed": 2.2,
		"lat": "36.78512545",
		"lng": "118.65236589",
		"yaw": 66.6,
		"followNum": 18,
		"navStrategy": 2,
		"obsX": 1.2,
		"obsY": -0.6,
		"obsDistance": 2.6,
		"gpsStatus": 4,
		"status": 501,
		"taskID": 1,
		"errorCode": 0,
		"emergencyCode": [
			0, 0, 0, 0, 0, 0, 0, 0
		],
		"deviceCode": 0,
		"vcuData": {
			"leftWheel": 180.0,
			"rightWheel": 190.0,
			"mpump": 5,
			"swingArm": 5,
			"emergency": 0,
			"mainLight": 0,
			"leftLight": 0,
			"rightLight": 0,
			"alert": 0,
			"mfLeft": 0,
			"mfUP": 0,
			"mfRight": 0,
			"mPush": 0,
			"mfFan": 0,
			"autoSpraying": 0,
			"automow": 0,
			"mode": 1,
			"mfFRate": 20,
			"mfCapacity": 22
		},
		"bmsData": {
			"hbc": 80.0,
			"hbv": 190.0,
			"hbpc": 180.0,
			"hbcc": 180.0,
			"lbc": 80.0,
			"lbv": 180.0,
			"lbpc": 180.0,
			"stateCode": 180.0
		},
		"obligate": {
			"obligate1": 0,
			"obligate2": 0,
			"obligate3": 0,
			"obligate4": 0,
			"obligate5": 0,
			"obligate6": 0,
			"obligate7": 0,
			"obligate8": 0
		}
	}
})";

    Mqtt_Report mqtt_report ;
	JsonHelper::JsonToObject(mqtt_report, data);
    int b=0;
    b++;
    return mqtt_report;
}