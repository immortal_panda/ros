//
// Created by lin on 2024/3/13.
//
/*
@filename: seed_log_local.cpp
@comment:
  specific code in log model
*/
#include "seed_log_local.h"
#include <exception>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/common.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/attributes/timer.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/attributes/current_thread_id.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/from_stream.hpp>
#include <boost/log/utility/setup/formatter_parser.hpp>
#include <boost/log/utility/formatting_ostream.hpp>
#include <boost/log/utility/setup/filter_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/log/sources/logger.hpp>
namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;

// ANSI颜色代码
const std::string log_color_reset = "\033[0m";
const std::string log_color_red = "\033[31m";        // 红色
const std::string log_color_green = "\033[32m";      // 绿色
const std::string log_color_yellow = "\033[33m";     // 黄色
const std::string log_color_blue = "\033[34m";       // 蓝色
const std::string plog_color_urple = "\033[35m";     // 紫色

SD_ULONG32 SeedLogLocal::InitLogSystem(SD_BOOL isconsole)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;

    if (isconsole == SD_TRUE)
    {
        logging::add_console_log(std::clog,
                                 keywords::format =
                                         (
                                                 expr::stream
                                                         << expr::attr< unsigned int >("LineID")
                                                         << " [" << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S.%f") << "]"
                                                         << " [" << expr::attr< SEED_LOG_LEVEL >("Severity") << "]"
                                                         << " [" << expr::attr< boost::log::aux::thread::id >("ThreadID") << "]"
                                                         << expr::smessage
                                         )
        );
    }
    // LineID是一个计数器，先创建一个初始值为1的计数器.
    boost::shared_ptr< logging::attribute > pCounter(new attrs::counter< unsigned int >(1));
    // 将它加入到全局属性中，如果要求将不同的内容输出到不同的日志文件中去，这里设置为全局属性可能就是不太合适了.
    logging::core::get()->add_global_attribute("LineID", *pCounter);
    // 下面是设置TimeStamp属性
    boost::shared_ptr< logging::attribute > pTimeStamp(new attrs::local_clock());
    logging::core::get()->add_global_attribute("TimeStamp", *pTimeStamp);
    boost::shared_ptr< logging::attribute > pNamedScope(new attrs::named_scope());
    logging::core::get()->add_thread_attribute("Scope", *pNamedScope);
    logging::add_common_attributes();
    //pSink->locked_backend()->auto_flush(true);
    return ulret;
}

SD_ULONG32 SeedLogLocal::LogInit(IN SeedLogCfg cfg)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    try{
        ulret = this->add_log_file(cfg);
        if (ulret != ERROR_BASE_SUCCEED){
            return ulret;
        }
        m_cfg = cfg;
    }
    catch (std::exception& e){
        //fail init exception
        std::cout << e.what() << std::endl;
    }
    return ulret;
}

SD_ULONG32 SeedLogLocal::add_log_file(IN SeedLogCfg cfg)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    //每天0点更新日志文件
    boost::shared_ptr<sinks::synchronous_sink< boost::log::sinks::text_file_backend> > pSink = logging::add_file_log
            (
                    keywords::open_mode = std::ios::app,
                    keywords::filter = (expr::attr< SEED_LOG_LEVEL >("Severity") >= SD_LOG_INFO && expr::attr< SD_ULONG32 >("Channel") == cfg.m_LogId),
                    keywords::file_name = cfg.m_path + "/" + cfg.m_filename + "_%Y%m%d-%N.log",
                    keywords::rotation_size = cfg.m_ulFileSize,
                    keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
                    keywords::format =
                            (
                                    expr::stream
                                            << "[" << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S.%f") << "]"
                                            << " [" << expr::attr< SEED_LOG_LEVEL >("Severity") << "]"
                                            << " [" << expr::attr< boost::log::aux::thread::id >("ThreadID") << "]"
                                            << expr::smessage
                            )
            );
    logging::core::get()->add_sink(pSink);
    pSink->locked_backend()->auto_flush(true);
    if (cfg.m_isDebug == SD_TRUE)
    {
        boost::shared_ptr<sinks::synchronous_sink< boost::log::sinks::text_file_backend> > pSink2 = logging::add_file_log
                (
                        keywords::open_mode = std::ios::app,
                        keywords::filter = (expr::attr< SEED_LOG_LEVEL >("Severity") <= SD_LOG_DEBUG && expr::attr< SD_ULONG32 >("Channel") == cfg.m_LogId),
                        keywords::file_name = cfg.m_path + "/" + cfg.m_filename + "_%Y%m%d.log.debug",
                        keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
                        keywords::format =
                                (
                                        expr::stream
                                                << "[" << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S.%f") << "]"
                                                << " [" << expr::attr< SEED_LOG_LEVEL >("Severity") << "]"
                                                << " [" << expr::attr< boost::log::aux::thread::id >("ThreadID") << "]"
                                                << expr::smessage
                                )
                );
        pSink2->locked_backend()->auto_flush(true);
        logging::core::get()->add_sink(pSink2);
        //keywords::filter = expr::attr< ULONG_32 >("Channel") == ID,
    }
    // LineID是一个计数器，先创建一个初始值为1的计数器.
    boost::shared_ptr< logging::attribute > pCounter(new attrs::counter< unsigned int >(1));
    // 将它加入到全局属性中，如果要求将不同的内容输出到不同的日志文件中去，这里设置为全局属性可能就是不太合适了.
    logging::core::get()->add_global_attribute("LineID", *pCounter);
    // 下面是设置TimeStamp属性
    boost::shared_ptr< logging::attribute > pTimeStamp(new attrs::local_clock());
    logging::core::get()->add_global_attribute("TimeStamp", *pTimeStamp);
    boost::shared_ptr< logging::attribute > pNamedScope(new attrs::named_scope());
    logging::core::get()->add_thread_attribute("Scope", *pNamedScope);
    logging::add_common_attributes();
    return ulret;
}

SD_VOID SeedLogLocal::log(IN SD_ULONG32 logid,IN SD_BOOL debug, IN int level, IN const char* filename, IN SD_ULONG32 line, IN std::string& message)
{
    if (level <= SD_LOG_DEBUG && debug == SD_FALSE){
        return;
    }
    //write_lock wlock(read_write_mutex);
    src::severity_channel_logger_mt< SEED_LOG_LEVEL, SD_ULONG32> slg(keywords::channel = logid);
    switch (level)
    {
        case SD_LOG_DEBUG:
            BOOST_LOG_SEV(slg, SD_LOG_DEBUG) << " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_KEY:
            BOOST_LOG_SEV(slg, SD_LOG_KEY) << " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_INFO:
            //info 绿色
            BOOST_LOG_SEV(slg, SD_LOG_INFO) << log_color_green<< " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_WARNING:
            //warning 黄色
            BOOST_LOG_SEV(slg, SD_LOG_WARNING) << log_color_yellow<< " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_ERROR:
            BOOST_LOG_SEV(slg, SD_LOG_ERROR) << log_color_red<< " [" << filename << ":" << line << "]" << message;
            break;
        default:
            BOOST_LOG_SEV(slg, SD_LOG_INFO) << " [" << filename << ":" << line << "]" << message;
            break;
    }
    //重置颜色。不然其他的输出颜色也变了
    std::cout << log_color_reset << std::endl;
}

SD_VOID SeedLogLocal::log(IN SD_ULONG32 logid ,IN SD_BOOL debug, IN int level, IN const char* filename, IN SD_ULONG32 line, IN const char* message)
{
    if (level <= SD_LOG_DEBUG && debug == SD_FALSE){
        return;
    }
    //write_lock wlock(read_write_mutex);
    src::severity_channel_logger_mt< SEED_LOG_LEVEL, SD_ULONG32> slg(keywords::channel = logid);
    switch (level)
    {
        case SD_LOG_DEBUG:
            BOOST_LOG_SEV(slg, SD_LOG_DEBUG) << " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_KEY:
            BOOST_LOG_SEV(slg, SD_LOG_KEY) << " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_INFO:
            BOOST_LOG_SEV(slg, SD_LOG_INFO) << log_color_green<< " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_WARNING:
            BOOST_LOG_SEV(slg, SD_LOG_WARNING) << log_color_yellow<< " [" << filename << ":" << line << "]" << message;
            break;
        case SD_LOG_ERROR:
            BOOST_LOG_SEV(slg, SD_LOG_ERROR) << log_color_red<< " [" << filename << ":" << line << "]" << message;
            break;
        default:
            BOOST_LOG_SEV(slg, SD_LOG_INFO) << " [" << filename << ":" << line << "]" << message;
            break;
    }
    //重置颜色。不然其他的输出颜色也变了
    std::cout << log_color_reset << std::endl;
}

SD_ULONG32 SeedLogLocal::GetLogId()
{
    return m_cfg.m_LogId;
}

SD_BOOL SeedLogLocal::GetDebug()
{
    return m_cfg.m_isDebug;
}
