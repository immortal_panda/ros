#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sstream>
#include "VX1000_Drv.h"
#include <fstream>
#include <iostream>
#include <string>
using namespace std;


const int MAX_vx1000_sub = 1024;
int DecArr_vx1000_sub[MAX_vx1000_sub] = { 0 };

/* VX1000用户协议数据帧 展示*/
VxNaviUserData_TypeDef m_VxNaviUserData_show;

/*msg callback function*/
void msgCallBack_ins(const system_msgs::vx1000_ins& msg)
{
    m_VxNaviUserData_show.GpsWeek_ins = msg.GpsWeek;
    m_VxNaviUserData_show.GpsWeekSecond_ins = msg.GpsWeekSecond;
    m_VxNaviUserData_show.SysTick = msg.SysTick;
    m_VxNaviUserData_show.NaviTick = msg.NaviTick;
    m_VxNaviUserData_show.InsLat = msg.InsLat;
    m_VxNaviUserData_show.InsLon = msg.InsLon;
    m_VxNaviUserData_show.InsAlt = msg.InsAlt;
    m_VxNaviUserData_show.InsVelN = msg.InsVelN;
    m_VxNaviUserData_show.InsVelE = msg.InsVelE;
    m_VxNaviUserData_show.InsVelD = msg.InsVelD;
    m_VxNaviUserData_show.InsRoll = msg.InsRoll;
    m_VxNaviUserData_show.InsPitch = msg.InsPitch;
    m_VxNaviUserData_show.InsHeading = msg.InsHeading;
    m_VxNaviUserData_show.cycle_cnt_ins = msg.cycle_cnt_ins;
    ROS_INFO("ins GpsWeek: %d",msg.GpsWeek);
}

void msgCallBack_imu(const system_msgs::vx1000_imu& msg)
{
    m_VxNaviUserData_show.GpsWeek_imu = msg.GpsWeek;
    m_VxNaviUserData_show.GpsWeekSecond_imu = msg.GpsWeekSecond;
    m_VxNaviUserData_show.GyrX = msg.GyrX;
    m_VxNaviUserData_show.GyrY = msg.GyrY;
    m_VxNaviUserData_show.GyrZ = msg.GyrZ;  
    m_VxNaviUserData_show.AccX = msg.AccX; 
    m_VxNaviUserData_show.AccY = msg.AccY;
    m_VxNaviUserData_show.AccZ = msg.AccZ;  
    m_VxNaviUserData_show.GyrTemp = msg.GyrTemp; 
    m_VxNaviUserData_show.AccTemp = msg.AccTemp; 
    m_VxNaviUserData_show.GyrSelfTestFlag = msg.GyrSelfTestFlag;
    m_VxNaviUserData_show.GyrConfigFlag = msg.GyrConfigFlag;
    m_VxNaviUserData_show.GyrRunStat = msg.GyrRunStat; 
    m_VxNaviUserData_show.GyrComuErr = msg.GyrComuErr;
    m_VxNaviUserData_show.GyrCRCErr = msg.GyrCRCErr;
    m_VxNaviUserData_show.GyrObtErr = msg.GyrObtErr;
    m_VxNaviUserData_show.GyrOverRange = msg.GyrOverRange;
    m_VxNaviUserData_show.GyrTempStat = msg.GyrTempStat;
    m_VxNaviUserData_show.AccSelfTestFlag = msg.AccSelfTestFlag;
    m_VxNaviUserData_show.AccConfigFlag = msg.AccConfigFlag;
    m_VxNaviUserData_show.AccRunStat = msg.AccRunStat;
    m_VxNaviUserData_show.AccComuErr= msg.AccComuErr;
    m_VxNaviUserData_show.AccCRCErr = msg.AccCRCErr;
    m_VxNaviUserData_show.AccObtErr = msg.AccObtErr;
    m_VxNaviUserData_show.AccOverRange = msg.AccOverRange;
    m_VxNaviUserData_show.AccTempStat = msg.AccTempStat;
    m_VxNaviUserData_show.cycle_cnt_imu = msg.cycle_cnt_imu;
    ROS_INFO("imu GpsWeek: %d",msg.GpsWeek);
    ROS_INFO("GyrX: %f",msg.GyrX);
}

void msgCallBack_gnss(const system_msgs::vx1000_gnss& msg)
{
    m_VxNaviUserData_show.GpsWeek_gnss = msg.GpsWeek;
    m_VxNaviUserData_show.GpsWeekSecond_gnss = msg.GpsWeekSecond;
    m_VxNaviUserData_show.UtcYear = msg.UtcYear;
    m_VxNaviUserData_show.UtcMonth = msg.UtcMonth;
    m_VxNaviUserData_show.UtcDay = msg.UtcDay;
    m_VxNaviUserData_show.UtcHour = msg.UtcHour;
    m_VxNaviUserData_show.UtcMinute = msg.UtcMinute;
    m_VxNaviUserData_show.UtcSecond = msg.UtcSecond;
    m_VxNaviUserData_show.UtcMillisecond = msg.UtcMillisecond;
    m_VxNaviUserData_show.GnssFixMode = msg.GnssFixMode;
    m_VxNaviUserData_show.GnssHeadMode = msg.GnssHeadMode;
    m_VxNaviUserData_show.GnssDiffAge = msg.GnssDiffAge;
    m_VxNaviUserData_show.GnssSatCnt = msg.GnssSatCnt;
    m_VxNaviUserData_show.GnssSatCntSlove = msg.GnssSatCntSlove;
    m_VxNaviUserData_show.GnssLat = msg.GnssLat;      
    m_VxNaviUserData_show.GnssLon = msg.GnssLon;    
    m_VxNaviUserData_show.GnssAlt = msg.GnssAlt;      
    m_VxNaviUserData_show.GnssTraceAngle = msg.GnssTraceAngle;
    m_VxNaviUserData_show.GnssHeading = msg.GnssHeading;
    m_VxNaviUserData_show.GnssSpeed = msg.GnssSpeed;
    m_VxNaviUserData_show.GnssVelN = msg.GnssVelN; 
    m_VxNaviUserData_show.GnssVelE = msg.GnssVelE;   
    m_VxNaviUserData_show.GnssVelD = msg.GnssVelD;   
    m_VxNaviUserData_show.GnssPDOP = msg.GnssPDOP;
    m_VxNaviUserData_show.GnssHDOP = msg.GnssHDOP;   
    m_VxNaviUserData_show.GnssVDOP = msg.GnssVDOP;
    m_VxNaviUserData_show.GnssLatStd = msg.GnssLatStd; 
    m_VxNaviUserData_show.GnssLonStd = msg.GnssLonStd; 
    m_VxNaviUserData_show.GnssHeadStd = msg.GnssHeadStd;
    m_VxNaviUserData_show.GnssSelfTestFlag = msg.GnssSelfTestFlag;
    m_VxNaviUserData_show.GnssConfigFlag = msg.GnssConfigFlag;
    m_VxNaviUserData_show.GnssRunStat = msg.GnssRunStat;
    m_VxNaviUserData_show.GnssComuStat = msg.GnssComuStat;
    m_VxNaviUserData_show.GnssPosStat = msg.GnssPosStat;
    m_VxNaviUserData_show.GnssHeadStat = msg.GnssHeadStat;
    m_VxNaviUserData_show.GnssRtcmStat = msg.GnssRtcmStat;
    m_VxNaviUserData_show.GnssAnt1Stat = msg.GnssAnt1Stat;
    m_VxNaviUserData_show.GnssAnt2Stat = msg.GnssAnt2Stat;
    m_VxNaviUserData_show.PPSStat = msg.PPSStat;
    m_VxNaviUserData_show.GpsTimeStat = msg.GpsTimeStat;
    m_VxNaviUserData_show.cycle_cnt_gnss = msg.cycle_cnt_gnss;
    ROS_INFO("gnss GpsWeek: %d",msg.GpsWeek);
}

void msgCallBack_stat(const system_msgs::vx1000_stat& msg)
{
    m_VxNaviUserData_show.GpsWeek_stat = msg.GpsWeek;
    m_VxNaviUserData_show.GpsWeekSecond_stat = msg.GpsWeekSecond;
    m_VxNaviUserData_show.SysTick_stat = msg.SysTick;
    m_VxNaviUserData_show.VolSup = msg.VolSup;
    m_VxNaviUserData_show.VolSys = msg.VolSys;
    m_VxNaviUserData_show.VolGnss = msg.VolGnss;
    m_VxNaviUserData_show.VolAnt = msg.VolAnt;
    m_VxNaviUserData_show.SysTemp = msg.SysTemp;
    m_VxNaviUserData_show.SysWorkStat = msg.SysWorkStat;
    m_VxNaviUserData_show.CoreStartUp = msg.CoreStartUp;
    m_VxNaviUserData_show.MonStartUp = msg.MonStartUp;
    m_VxNaviUserData_show.ParaReadFlag = msg.ParaReadFlag;
    m_VxNaviUserData_show.VolSupStat = msg.VolSupStat;
    m_VxNaviUserData_show.VolSysStat = msg.VolSysStat;
    m_VxNaviUserData_show.VolGnssStat = msg.VolGnssStat;
    m_VxNaviUserData_show.VolAntStat = msg.VolAntStat;
    m_VxNaviUserData_show.VolMcuStat = msg.VolMcuStat;
    m_VxNaviUserData_show.VolImuStat = msg.VolImuStat;
    m_VxNaviUserData_show.VolCanStat = msg.VolCanStat;
    m_VxNaviUserData_show.EthInitFlag = msg.EthInitFlag;
    m_VxNaviUserData_show.EthDHCPStat = msg.EthDHCPStat;
    m_VxNaviUserData_show.EthLinkStat = msg.EthLinkStat;
    m_VxNaviUserData_show.InsNavStat = msg.InsNavStat;
    m_VxNaviUserData_show.GyrStat_stat = msg.GyrStat;
    m_VxNaviUserData_show.AccStat_stat = msg.AccStat;
    m_VxNaviUserData_show.GnssStat_stat = msg.GnssStat;
    m_VxNaviUserData_show.OdomStat_stat = msg.OdomStat;
    m_VxNaviUserData_show.cycle_cnt_state = msg.cycle_cnt_state;
    ROS_INFO("stat GpsWeek: %d",msg.GpsWeek);
}

void msgCallBack_cov(const system_msgs::vx1000_cov& msg)
{
    m_VxNaviUserData_show.GpsWeek_cov = msg.GpsWeek;
    m_VxNaviUserData_show.GpsWeekSecond_cov = msg.GpsWeekSecond;
    m_VxNaviUserData_show.InsLatErr = msg.InsLatErr;
    m_VxNaviUserData_show.InsLonErr = msg.InsLonErr;
    m_VxNaviUserData_show.InsAltErr = msg.InsAltErr;
    m_VxNaviUserData_show.InsLatConf = msg.InsLatConf;
    m_VxNaviUserData_show.InsLonConf = msg.InsLonConf;
    m_VxNaviUserData_show.InsAltConf = msg.InsAltConf;
    m_VxNaviUserData_show.InsHeadingErr = msg.InsHeadingErr;
    m_VxNaviUserData_show.InsPitchErr = msg.InsPitchErr;
    m_VxNaviUserData_show.InsRollErr = msg.InsRollErr;
    m_VxNaviUserData_show.InsHeadingConf = msg.InsHeadingConf;
    m_VxNaviUserData_show.InsPitchConf = msg.InsPitchConf;
    m_VxNaviUserData_show.InsRollConf = msg.InsRollConf;
    m_VxNaviUserData_show.InsVelNErr = msg.InsVelNErr;
    m_VxNaviUserData_show.InsVelEErr = msg.InsVelEErr;
    m_VxNaviUserData_show.InsVelDErr = msg.InsVelDErr;
    m_VxNaviUserData_show.InsVelNConf = msg.InsVelNConf;
    m_VxNaviUserData_show.InsVelEConf = msg.InsVelEConf;
    m_VxNaviUserData_show.InsVelDConf = msg.InsVelDConf;
    m_VxNaviUserData_show.InsCaliProg = msg.InsCaliProg;
    m_VxNaviUserData_show.cycle_cnt_cov = msg.cycle_cnt_cov;
    ROS_INFO("cov GpsWeek: %d",msg.GpsWeek);
}

void msgCallBack_odom(const system_msgs::vx1000_odom& msg)
{
    m_VxNaviUserData_show.GpsWeek_odom = msg.GpsWeek;
    m_VxNaviUserData_show.GpsWeekSecond_odom = msg.GpsWeekSecond;
    m_VxNaviUserData_show.OdomGear = msg.OdomGear;
    m_VxNaviUserData_show.WheelSpdFl = msg.WheelSpdFl;
    m_VxNaviUserData_show.WheelSpdFr = msg.WheelSpdFr;
    m_VxNaviUserData_show.WheelSpdRl = msg.WheelSpdRl;
    m_VxNaviUserData_show.WheelSpdRr = msg.WheelSpdRr;
    m_VxNaviUserData_show.VehicleSpd = msg.VehicleSpd;
    m_VxNaviUserData_show.WheelPulseFl = msg.WheelPulseFl;
    m_VxNaviUserData_show.WheelPulseFr = msg.WheelPulseFr;
    m_VxNaviUserData_show.WheelPulseRl = msg.WheelPulseRl;
    m_VxNaviUserData_show.WheelPulseRr = msg.WheelPulseRr;
    m_VxNaviUserData_show.SteerWheelAngle = msg.SteerWheelAngle;
    m_VxNaviUserData_show.SteerWheelSpeed = msg.SteerWheelSpeed;
    m_VxNaviUserData_show.WheelScaleL = msg.WheelScaleL;
    m_VxNaviUserData_show.WheelScaleR = msg.WheelScaleR;
    m_VxNaviUserData_show.OdomRunStat = msg.OdomRunStat;
    m_VxNaviUserData_show.GearErr = msg.GearErr;
    m_VxNaviUserData_show.WheelSpdErr = msg.WheelSpdErr;
    m_VxNaviUserData_show.VehicleSpdErr = msg.VehicleSpdErr;
    m_VxNaviUserData_show.WheelPulseErr = msg.WheelPulseErr;
    m_VxNaviUserData_show.SteerWheelErr = msg.SteerWheelErr;
    m_VxNaviUserData_show.cycle_cnt_odom = msg.cycle_cnt_odom;
    ROS_INFO( "doom GpsWeek: d%",msg.GpsWeek);
}

int main(int argc, char** argv)
{
    setlocale(LC_ALL,"");//设置中文输出环境
    ros::init(argc, argv,"vx1000_sub");
    ros::NodeHandle nd;
     ROS_INFO("接收启动");
    /*publish string msg, 100 is the catch queue*/
    ros::Subscriber sub_ins = nd.subscribe("VX1000_ins", 1024, msgCallBack_ins);
    ros::Subscriber sub_imu = nd.subscribe("VX1000_imu", 1024, msgCallBack_imu);
    ros::Subscriber sub_gnss = nd.subscribe("VX1000_gnss", 1024, msgCallBack_gnss);
    ros::Subscriber sub_stat = nd.subscribe("VX1000_stat", 1024, msgCallBack_stat);
    ros::Subscriber sub_cov = nd.subscribe("VX1000_cov", 1024, msgCallBack_cov);
    ros::Subscriber sub_odom = nd.subscribe("VX1000_odom", 1024, msgCallBack_odom);
    ros::spin();
    return 0;
}
