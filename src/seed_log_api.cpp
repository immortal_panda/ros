//
// Created by lin on 2024/3/13.
//
/*
@filename: seed_log_api.cpp
@comment:
Log interface
*/
#include "seed_log_api.h"
#include "seed_log_local.h"
#include <boost/foreach.hpp>
#include <iostream>

SeedLogSimpleManager * SeedLogSimpleManager::s_instance = nullptr;

SD_VOID SeedLogSimpleManager::log_write(IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, ...)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    SeedLogSimpleManager& ref_instance = SeedLogSimpleManager::instance();
    if (ref_instance.m_log)
    {
        char message[MAXLOGLEN + 1] = { 0 };
        va_list vArgList;
        va_start(vArgList, fmt);
        vsnprintf(message, MAXLOGLEN, fmt, vArgList);
        va_end(vArgList);
        ref_instance.m_log->log(ref_instance.m_config.m_LogId, ref_instance.m_config.m_isDebug, level, MFILENAME(filename), lineid, message);
    }
    return ;
}

SD_LONG32 SeedLogSimpleManager::callback_write(IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, va_list va)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    SeedLogSimpleManager& ref_instance = SeedLogSimpleManager::instance();
    if (ref_instance.m_log)
    {
        char message[MAXLOGLEN + 1] = { 0 };
        vsnprintf(message, MAXLOGLEN, fmt, va);
        ref_instance.m_log->log(ref_instance.m_config.m_LogId, ref_instance.m_config.m_isDebug, level, MFILENAME(filename), lineid, message);
        return ERROR_BASE_SUCCEED;
    }
    return ulret;
}

SD_ULONG32 SeedLogSimpleManager::SeedLogSimpleInit(IN SeedLogCfg config)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    m_log = new SeedLogLocal();
    ulret = m_log->LogInit(config);
    m_config = config;
    m_config.m_LogId = m_log->GetLogId();
    return ulret;
}

SeedLogSimpleManager& SeedLogSimpleManager::instance()
{
    if (s_instance == nullptr)
    {
        s_instance = new SeedLogSimpleManager;
    }
    return *s_instance;
}

SD_ULONG32 SeedLogSimpleManager::LogSystemInit(SD_BOOL isconsole)
{
    try{
        return SeedLogApi::LogSystemInit(isconsole);
    }
    catch (std::exception & e){
        std::cout << e.what() << std::endl;
        return ERROR_BASE_FALT;
    }
}

SD_ULONG32 SeedLogApi::LogSystemInit(SD_BOOL isconsole)
{
    try{
        
        return SeedLogLocal::InitLogSystem(isconsole);

    }
    catch (std::exception & e){
        std::cout << e.what() << std::endl;
        return ERROR_BASE_FALT;
    }
}


SeedLogMutilpleManager * SeedLogMutilpleManager::s_instance=nullptr;

SeedLogMutilpleManager& SeedLogMutilpleManager::instance()
{
    if (s_instance == nullptr)
    {
        s_instance = new SeedLogMutilpleManager;
    }
    return *s_instance;
}

SD_ULONG32 SeedLogMutilpleManager::LogSystemInit(SD_BOOL isconsole)
{
    return SeedLogApi::LogSystemInit(isconsole);
}

SD_ULONG32 SeedLogMutilpleManager::SeedLogMutilpleInit(IN std::vector<SeedLogCfg>& configs)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    try{

        BOOST_FOREACH(auto& id, configs){
                        SeedLogLocal* ptr = new SeedLogLocal();
                        id.m_LogId = ++m_adapter_id;
                        ulret = ptr->LogInit(id);
                        if (ulret != ERROR_BASE_SUCCEED){
                            return ulret;
                        }
                    }
    }
    catch (std::exception & e){
        return ERROR_BASE_FALT;
    }
    return ulret;
}

SD_VOID SeedLogMutilpleManager::log_write(IN SD_ULONG32 logid,IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, ...)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    SeedLogMutilpleManager& ref_instance = SeedLogMutilpleManager::instance();
    if (ref_instance.m_logs.find(logid) != ref_instance.m_logs.end() &&
        ref_instance.m_logs[logid] != nullptr)
    {
        char message[MAXLOGLEN + 1] = { 0 };
        va_list vArgList;
        va_start(vArgList, fmt);
        vsnprintf(message, MAXLOGLEN, fmt, vArgList);
        va_end(vArgList);
        ref_instance.m_logs[logid]->log(ref_instance.m_logs[logid]->GetLogId(), ref_instance.m_logs[logid]->GetDebug(), level, MFILENAME(filename), lineid, message);
    }
    return;
}

SD_LONG32 SeedLogMutilpleManager::callback_write(IN SD_ULONG32 logid,IN SD_ULONG32 level, const char* filename, SD_ULONG32 lineid, const char* fmt, va_list va)
{
    SD_ULONG32 ulret = ERROR_BASE_SUCCEED;
    SeedLogMutilpleManager& ref_instance = SeedLogMutilpleManager::instance();
    if (ref_instance.m_logs.find(logid) != ref_instance.m_logs.end() &&
        ref_instance.m_logs[logid] != nullptr)
    {
        char message[MAXLOGLEN + 1] = { 0 };
        vsnprintf(message, MAXLOGLEN, fmt, va);
        ref_instance.m_logs[logid]->log(ref_instance.m_logs[logid]->GetLogId(), ref_instance.m_logs[logid]->GetDebug(), level, MFILENAME(filename), lineid, message);
        return ERROR_BASE_SUCCEED;
    }
    return ulret;
}

