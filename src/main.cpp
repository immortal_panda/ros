#include <stdio.h>
#include <stdbool.h>
#include <ros/ros.h>
#include "Rec_Thread.h"
#include "VX1000_Drv.h"
#include <mqtt/async_client.h>
#include <string>
#include "mqtt_receive_class.h"
#include "mqtt_test.h"
#include <ctime>
#include <chrono>
#include <iomanip>
#include "my_mqtt_client.hpp"
#include "Common.h"
#include <csignal>
#include <unistd.h>
#include <signal.h>

#include <execinfo.h>

#include "Robot.h"
using namespace aigc;

#define VX1000_BUFF_SIZE 10240 

uint8_t vx1000_data_buff[VX1000_BUFF_SIZE];
#define APP_VERSION "1.0.0"
#define APP_UPDATE_TIME "2024-05-02 08:40"
#define APP_UPDATA_DESCRIBE "初版"


extern Rec_Queue VcuRecQueue;




long long void_to_longlong(void* ptr) {
    long long value;
    memcpy(&value, &ptr, sizeof(long long));
    return value;
}

 
void signal_handler(int signal) {
    std::cout << "Caught signal " << signal << ", saving data..." << std::endl;
    
    // 这里写上保存数据的逻辑
    // 例如，将"Data was saved"写入到文件
    void *array[10];
    size_t size;
 
    // 获取所有调用的地址
    size = backtrace(array, 10);
    FILE * fd = fopen("crash.log", "w");
    if (fd ) {
        fprintf(fd, "Signal: %d\n", signal);
        backtrace_symbols_fd(array, size, fileno(fd));


        char **strings = backtrace_symbols(array, size);
         // 使用addr2line工具解析地址，获取行号等信息
        
        for (int i = 0; i < size; i++) {
            printf("%s\n", strings[i]);
            char cmd[256];
            auto addr = (void_to_longlong(array[i]));
            void* ptr = (void*)addr;
            snprintf(cmd, sizeof(cmd), "addr2line %p -e %s -f", ptr, "vx1000_drv");
            system(cmd);
            fprintf(fd, "cmd: %s\n", cmd);
        }
    
        
    }

 
    exit(signal);


	
 
    

}





int main(int argc, char **argv)
{
	setlocale(LC_ALL,"");//设置中文输出环境

    
	 // 注册信号处理函数
    struct sigaction sa;
    sa.sa_handler = &signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;

	
	signal(SIGSEGV, signal_handler); // 段错误信号
    signal(SIGABRT, signal_handler); // 异常终止信号

  
	seed_log_example::seed_log_test();
    SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----系统初始化-----");
    SEED_EXAMPLE_LOG2(SD_LOG_INFO, "-----系统版本%s---系统更新描述:\r\n %s--",APP_VERSION,APP_UPDATA_DESCRIBE);

	SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----读取系统配置文件-----");
	if (Common::isFileExists("SystemSetting.json"))
	{
		SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----读取配置文件中-----");
        Common::_system_setting= Common::GetLocationJson<SystemInfo>("SystemSetting.json");
		SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----读取配置文件完成-----");
	}
	else
	{
		SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----系统文件不存在，启用默认创建-----");
		Common::_system_setting.imei="9257";
		Common::_system_setting.mqtt_username="dm";
		Common::_system_setting.mqtt_pw="dm123";
        Common::_system_setting.mqtt_server="mqtt://123.249.103.132:1883";
		Common::_system_setting.RefreshFre=20;
		Common::_system_setting.vcu_com_port_name="/dev/ttyUSB1";
		Common::_system_setting.vx1000_com_port_name="/dev/ttyUSB0";
		Common::SaveJsonToLocation(Common::_system_setting,"SystemSetting.json");
	}


    if (Common::isFileExists("task.json"))
	{
		SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----读取任务文件中-----");
        Common::_RobotTask= Common::GetLocationJson<RobotTask>("task.json");
		SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----读取任务文件完成-----");
	}
	else
	{
		SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----任务文件不存在-----");
	}
    
    
	
	ros::init(argc, argv, "vx1000_pub");
	
	ros::NodeHandle node;

	SEED_EXAMPLE_LOG0(SD_LOG_INFO, "ros 初始化完成");
	Rec_Queue RecQueue(vx1000_data_buff, sizeof(vx1000_data_buff));
	VX1000_Drv VX1000(node,RecQueue);
	Rec_Thread SerialRec_Thread(node, RecQueue);

    
    
    Robot::getInstance();
    Vcu_Thread vcuSendRec_Thread(VcuRecQueue);
	ros::Rate loop_rate(Common::_system_setting.RefreshFre);

	SEED_EXAMPLE_LOG0(SD_LOG_INFO, "启动VX1000节点");

	SEED_EXAMPLE_LOG0(SD_LOG_INFO, "启动MQTT功能块");


    
    
    // string data = R"({
    // "msgType": "robot",
    // "productCode": "9257",
    // "type": 1001,
    // "data": {
    //     "waypoint": "1 108.235396 23.031899,2 108.235288 23.032189,3 108.235369 23.031899,4 108.235272 23.032185",
    //     "pathID": 1871
    // }
    // })";
    // MqttClient::ParaseTask(data);
    
    
    // // mqtt 连接本地
    MqttClient::connect(Common::_system_setting.mqtt_server, "clientId");
	//MqttClient::connect("mqtt://123.249.103.132:1883", "clientId");
	if (MqttClient::isConnect)
	{
		std::string str = (boost::format("ROBOT/%1%") % Common::_system_setting.imei.c_str() ).str();
		SEED_EXAMPLE_LOG1(SD_LOG_INFO, "MQTT %s",str.c_str());
		MqttClient::subscribe("SERVICE/ROBOT_KEEPLIVE",MqttClient::ParaseServerMessage);
		MqttClient::subscribe(str,MqttClient::ParaseServerMessage);
	}
	
	

	


	while (ros::ok())
	{

		ros::spinOnce();

		if (SerialRec_Thread.GetOpenState())
		{
			/* code */
			SerialRec_Thread.SerialRec_Thread();

			VX1000.Parase();

		}

        if (vcuSendRec_Thread.GetOpenState())
		{
			/* code */
            //发送数据
            Robot::getInstance()->SendDataToVcu();
            vcuSendRec_Thread.SerialSend_Thread();

            //接收数据
			vcuSendRec_Thread.SerialRec_Thread();
			Robot::getInstance()->Parase();
            
		}
		Robot::getInstance()->SportsStrategy();
		
		loop_rate.sleep();

		
	}
}
