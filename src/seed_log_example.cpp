//
// Created by lin on 2024/3/13.
//
/*
@filename: seed_log_example.cpp
@comment:
		log example
*/

#include "seed_log_example.h"
#include <iostream>
#include <string>
#include <codecvt>
#include <locale>



//std::string seed_log_example::wstring_to_string(const std::wstring& wstr) {
//    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
//    return converter.to_bytes(wstr);
//}

SD_ULONG32 seed_log_example::seed_log_test()
{
    try
    {
        //初始化日志系统(global配置)
        SeedLogSimpleManager::LogSystemInit(SD_TRUE);
        SeedLogCfg config;
        config.m_path = std::string("./log");
        config.m_filename = std::string("VX1000D");
        config.m_isDebug = SD_TRUE;
        config.m_LogId = 0;
        s_instance.SeedLogSimpleInit(config);

        //测试代码：
        //SEED_EXAMPLE_LOG1(SD_LOG_INFO, "log init path [%s]", config.m_path.c_str());
        //SEED_EXAMPLE_LOG2(SD_LOG_INFO, "log init path [%s] filename [%s]", config.m_path.c_str(), config.m_filename.c_str());
        //SEED_EXAMPLE_LOG2(SD_LOG_INFO, "log init path debug [%u]  log id[%u]", config.m_isDebug, config.m_LogId);
        //....
    }
    catch (std::exception &e){
        std::cout << e.what() << std::endl;
    }
    return ERROR_BASE_SUCCEED;
}

SD_ULONG32 seed_log_example::seed_log(std::string str)
{
    try
    {
        //测试代码：
        SEED_EXAMPLE_LOG1(SD_LOG_INFO, "log init path [%s]", str.c_str());
        //....
    }
    catch (std::exception &e){
        std::cout << e.what() << std::endl;
    }
    return ERROR_BASE_SUCCEED;
}

SD_ULONG32 seed_log_example::seed_wlog(std::wstring str)
{
    try
    {
        //测试代码：
        SEED_EXAMPLE_LOG1(SD_LOG_INFO, "log init path [%s]", wstring_to_string(str).c_str());
        //....
    }
    catch (std::exception &e){
        std::cout << e.what() << std::endl;
    }
    return ERROR_BASE_SUCCEED;
}

std::string seed_log_example::wstring_to_string(std::wstring wstr) {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return converter.to_bytes(wstr);
}
