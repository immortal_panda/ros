#include "Common.h"
#include "my_mqtt_client.hpp"
using namespace std;


//类外初始化 静态成员
TaskDataClass Common::_taskDataJson;
map<string,Gps_Data> Common::navigation_data;
SystemInfo Common::_system_setting;
mqtt::async_client* MqttClient::client = nullptr;
boost::thread MqttClient::roport_robot_data_thread;
bool MqttClient::isConnect;
RobotTask Common::_RobotTask;
Robot_ErrorType Common::_RobotError;







bool Common::isFileExists(string filePath)
{
    return (access(filePath.c_str(), F_OK) == 0);
}


template<typename T>
tuple<bool,T> Common::hasDuplicateId(const std::list<T>& lst, const int& id) {

    auto item = std::find_if(lst.begin(), lst.end(), [&](const T& item) {
        return item.id == id;
    });
    return std::make_tuple(item != lst.end(), item);
}


string Common::GetStringFormTimestamp(u_int64_t _time)
{
    //秒级别
    boost::posix_time::ptime time_pt = boost::posix_time::from_time_t(_time);
            
    // 将 ptime 对象转换为字符串
    std::string time_str = boost::posix_time::to_iso_string(time_pt);
    return time_str;
}

string Common::GetCurTimeString()
{
    auto now = std::chrono::system_clock::now();
    std::time_t now_time = std::chrono::system_clock::to_time_t(now - std::chrono::hours(24));
    std::stringstream ss;
    ss << std::put_time(std::localtime(&now_time), "%Y-%m-%d %X");
    return ss.str();
}

time_t Common::GetTimeStampFormString(string timeStr){
    struct tm timeinfo;
    timeinfo.tm_isdst=-1;
    strptime(timeStr.c_str(), "%Y-%m-%d %H:%M:%S",  &timeinfo);
    time_t timeStamp = mktime(&timeinfo);
    printf("timeStamp=%ld\n",timeStamp);
    return timeStamp;
}

time_t Common::GetCurTimeStamp()
{
    string _t = Common::GetCurTimeString();
    return Common::GetTimeStampFormString(_t);
}


uint16_t Common::calculateModbusCRC(const unsigned char* buffer, size_t bufferSize) {
    // boost::crc_basic<16> result(0x8005, 0xFFFF, 0, false, false); // 使用 MODBUS CRC 标准

    // result.process_bytes(buffer, bufferSize);

    // return result.checksum();
    //CRC16校验
    uint16_t crc = 0xFFFF;
    for (size_t i = 0; i < bufferSize; i++) {
        crc ^= buffer[i];
        for (size_t j = 0; j < 8; j++) {
            if ((crc & 0x0001) == 0x0001) {
                crc >>= 1;
                crc ^= 0xA001;
            } else {
                crc >>= 1;
            }
        }
    }
    return crc;
}

bool Common::isBitSet(unsigned char byte, int bitPosition) {
    //从右到左，0-7
    return (byte & (1 << bitPosition)) != 0;
}

double Common::GetRad(double d)
{
    return (d * 3.14159265358979) / 180.0;
}

double Common::MygetDistance(const Gps_Data& sp, const Gps_Data& ep) {
    if (sp.lat != 0 && ep.lat != 0) {
        
        double radLat1 = GetRad(sp.lat);
        double radLat2 = GetRad(ep.lat);
        double a = radLat1 - radLat2;
        double b = GetRad(sp.lon) - GetRad(ep.lon);
        double s = 2 * asin(sqrt(pow(sin(a / 2), 2) + cos(radLat1) * cos(radLat2) * pow(sin(b / 2), 2)));
        s = s * 6378137; // 地球半径(米)
        return s;
    }
    return 0;   
}

//假设sp和ep是两个Point对象，d是总距离，r是步长，c是布尔值
/**
     * 根据总距离和步长在两个经纬度坐标点之间补点
     * @param  {Object} sp 起点坐标点
     * @param  {Object} ep 终点坐标点
     * @param  {Number} d 总距离(米)
     * @param  {Number} r 步长(米)
     * @param  {Boolean} c 输出是否包含起终点
     * @return {Array} 补点后的坐标数组(包含起点和终点)
     */
 std::vector<Gps_Data> Common::getFillPoints(const Gps_Data& sp, const Gps_Data& ep, double d, double r, bool c) {
     std::vector<Gps_Data> points;
     double lngDiff = ep.lon - sp.lon;
     double latDiff = ep.lat - sp.lat;
     int n = std::ceil(d / r);
     double a = lngDiff / n;
     double b = latDiff / n;
     for (int i = 1; i < n; i++) {
         double lng = sp.lon + a * i;
         double lat = sp.lat + b * i;
         Gps_Data point;
         point.lat = lat;
         point.lon = lng;
         points.push_back(point);
     }
     if (c) {
         points.insert(points.begin(), sp);
         points.push_back(ep);
     }
     return points;
 }

double Common::getAngle(const Gps_Data& position1, const Gps_Data& position2) {
    double w1 = position1.lat / 180 * M_PI;
    double j1 = position1.lon / 180 * M_PI;

    double w2 = position2.lat / 180 * M_PI;
    double j2 = position2.lon / 180 * M_PI;

    double ret = 4 * pow(sin((w1 - w2) / 2), 2) - pow(sin((j1 - j2) / 2) * (cos(w1) - cos(w2)), 2);
    ret = sqrt(ret);

    double temp = sin(abs(j1 - j2) / 2) * (cos(w1) + cos(w2));
    ret = ret / temp;

    ret = atan(ret) / M_PI * 180;
    ret += 90;

   
    if (j1 - j2 < 0) {

        if (w1 - w2 < 0) {
            // console.log('w1<w2')
            ret;
        } else {
            // console.log('w1>w2')
            ret = -ret + 180;
        }
    } else {
        // console.log('j1>j2')
        if (w1 - w2 < 0) {
            // console.log('w1<w2')
            ret = 180 + ret;
        } else {
            // console.log('w1>w2')
            ret = -ret;
        }
    }
    return ret;
    }


    std::list<double> Common::getDistanceList(const std::list<Gps_Data>& points) {
     std::list<double> distances;
     for (size_t i = 0; i < points.size() - 1; i++) {
         const Gps_Data sp = *(std::next(points.begin(), i));
         const Gps_Data ep = *(std::next(points.begin(), i+1));
         double distance = MygetDistance(sp, ep);
         distances.push_back(distance);
     }
     return distances;
 }




//得到一个Point到一个list<Point>的所有距离
std::list<double> Common::getDistanceList(const Gps_Data& point, const std::list<Gps_Data>& points) {
    std::list<double> distances;
    for (const auto& p : points) {
        double distance = MygetDistance(point, p);
        distances.push_back(distance);
    }
    return distances;
}


//list<double> distances,求列表内相邻元素的和，组成一个新的列表
std::list<double> Common::getAdjacentSumList(const std::list<double>& distances) {
    std::list<double> sums;
    double sum = 0;
    for (size_t i = 0; i < distances.size()-1; i++)
    {
        /* code */
        //取distances第i个元素和第i+1个元素的和

        sums.push_back(*(std::next(distances.begin(), i)) + *(std::next(distances.begin(), i + 1)));
    }
    
    return sums;
}

//获取最近点index的集合
std::list<int> Common::getNearbyIndexList(const Gps_Data& point, const std::list<Gps_Data>& points, double distance=0.5) {
    std::list<int> nearbyIndexList;
    //获取点point到所有点的距离
    std::list<double> distances = getDistanceList(point, points);
    //获取poinsts两点之间的距离
    std::list<double> road_distances = getDistanceList(points);
    //获取distances的相邻元素的和
    std::list<double> adjacentSumList = getAdjacentSumList(distances);
    //判断distances点的个数是否和road_distances相等
    if (adjacentSumList.size() != road_distances.size()) {
        return nearbyIndexList;
    }
    //遍历adjacentSumList，判断是否大于distance
    for (size_t i = 0; i < adjacentSumList.size(); i++) {
        double distance_offset = sqrt(distance * distance*4 + (*(std::next(road_distances.begin(), i))) * (*(std::next(road_distances.begin(), i))));
        if ((*(std::next(adjacentSumList.begin(), i))) < distance_offset) {
            nearbyIndexList.push_back(i);
        }
    }
    //获取距离point最近的点的index集合
    return nearbyIndexList;
        
}

std::tuple <bool, double> Common::isYaw(const Gps_Data& point,const Gps_Data& startPoint,const Gps_Data& endPoint,double distance=0.5)
{
    
    double curAngle = getAngle(point, endPoint);
    double sumDis = MygetDistance(point, startPoint) + MygetDistance(point, endPoint);
    double TheoreticalDistance = MygetDistance(startPoint,endPoint);
    double distance_max = sqrt(distance * distance*4 + TheoreticalDistance * TheoreticalDistance);
    if (sumDis < distance_max) {
        return std::make_tuple(false, curAngle);//没有偏航
    }
    return std::make_tuple(true, curAngle);
    
}











    


 

 
 