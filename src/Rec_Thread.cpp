#include "Rec_Thread.h"
#include "Common.h"

using namespace std;

Rec_Thread::Rec_Thread(ros::NodeHandle &nh, Rec_Queue &q) : node_handle(nh), queue(q)
{
    // _serial_rec_thread = new (std::nothrow) std::thread(&Rec_Thread::SerialRec_Thread, this);

    ros::param::param<std::string>("xv1000_drv/port_name", this->port_name, std::string(Common::_system_setting.vx1000_com_port_name.c_str()));
	ros::param::param<int>("xv1000_drv/port_baudrate", this->baudrate, 460800);
    try
    {
        /* code */
        this->Serial_Open();
    }
    catch(const std::exception& e)
    {
        //std::cerr << e.what() << '\n';
        
        SEED_EXAMPLE_LOG2(SD_LOG_ERROR, "-----串口%s打开失败[%s]-----",Common::_system_setting.vx1000_com_port_name.c_str(),e.what());
        this->Serial_Open_State=false;
        Common::_RobotError.bit.Three_Line_Radar_Connect_Error=true;
    }
    
    
}

Rec_Thread::~Rec_Thread()
{
    try
    {
        /* code */
        _serial.flushInput();
        _serial.flushOutput();
        // _serial.flush();
        _serial.close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    
}
bool Rec_Thread::GetOpenState()
{
    return this->Serial_Open_State;
}

bool Rec_Thread::Serial_Open()
{
	_serial.setPort(this->port_name);
	_serial.setBaudrate(this->baudrate);

	serial::Timeout to = serial::Timeout::simpleTimeout(1000);
	_serial.setTimeout(to);

	_serial.open();

	if (!_serial.isOpen())
	{
        SEED_EXAMPLE_LOG0(SD_LOG_ERROR,"VX1000 串口打开失败")
		Serial_Open_State = false;
        return false;
	}
    
    SEED_EXAMPLE_LOG0(SD_LOG_INFO,"VX1000 串口打开成功")
	Serial_Open_State = true;
	return true;
}


void Rec_Thread::SerialRec_Thread()
{
    string rec_tmp;

    rec_tmp.clear();
    uint16_t i = 0;

    // while (1)
    // {
        if (_serial.available())
        {
            try
            {
                rec_tmp =_serial.read(_serial.available());
            }
            catch (std::out_of_range const &exc)
            {
                std::cout << exc.what() << std::endl;
                // continue;
            }

            if(rec_tmp.size() > 0)
            {
               if(((this->queue.GetState() == RecQueue_ok) || (this->queue.GetState() ==RecQueue_empty )))
                {
                    this->queue.Push((uint8_t *)(rec_tmp.c_str()), rec_tmp.size());
                }
                else
                {
                    printf("queue state:\t%d\r\n", this->queue.GetState());
                    printf("RecQueue_ok\t\t----%d\r\n", RecQueue_ok);
                    printf("RecQueue_overflow_w\t----%d\r\n", RecQueue_overflow_w);
                    printf("RecQueue_overflow_r\t----%d\r\n", RecQueue_overflow_r);
                    printf("RecQueue_empty\t\t----%d\r\n", RecQueue_empty);
                    printf("RecQueue_full\t\t----%d\r\n", RecQueue_full);
                    printf("RecQueue_obj_error\t----%d\r\n", RecQueue_obj_error);

                    printf("queue size :\t%d\r\n", this->queue.GetSize());
                    printf("queue head:\t%d\r\n",this->queue.Head());
                    printf("queue end:\t%d\r\n",this->queue.End());

                    printf("queue push failed Thread Halt\r\n");
                    while(1);
                }
            }

            rec_tmp.clear();
        }
    // }
}
