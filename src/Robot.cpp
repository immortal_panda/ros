#include "Robot.h"
#include <system_msgs/vx1000_gnss.h>

using namespace std;

uint8_t vcu_data_buff[VCU_BUFF_SIZE];
    //VCU数据解析
Rec_Queue VcuRecQueue(vcu_data_buff, sizeof(vcu_data_buff));
extern system_msgs::vx1000_gnss vx1000_gnss_data;
//第一次调用时候，创建实例对象
Robot* Robot::instance_ = new (std::nothrow) Robot(VcuRecQueue);
PIDController AnglePid(0.5, 0.2, 0.1);
#define DecodeVCUSize 1




Robot* Robot::getInstance() {
    return instance_;
}

Robot::Robot(Rec_Queue &q) : queue(q)
{
    
}

Robot::~Robot()
{

}

void Robot::Decode()
{
 
    uint8_t VCU_DATA_BUFFER_TEMP[VCU_PROTO_LEN*DecodeVCUSize] = {0};
    uint8_t VCU_DATA_BUFFER[DecodeVCUSize][VCU_PROTO_LEN] = {0};
	

    if(this->queue.GetSize() < sizeof(VCU_DATA_BUFFER_TEMP))
        return;
    
    this->queue.Pop(VCU_DATA_BUFFER_TEMP, sizeof(VCU_DATA_BUFFER_TEMP));

    uint8_t vcu_size = sizeof(VCU_DATA_BUFFER_TEMP)/VCU_PROTO_LEN;

    uint32_t k = 0;
    for(uint8_t i = 0; i < vcu_size; i++)
    {
        for(uint8_t j = 0;j<VCU_PROTO_LEN;j++)
        {
            VCU_DATA_BUFFER[i][j] = VCU_DATA_BUFFER_TEMP[k++]; 
        }
    }

    for(uint8_t i = 0 ; i< (sizeof(VCU_DATA_BUFFER_TEMP)/VCU_PROTO_LEN) ;i++ )
    {
        Proto_INSP_UnPack_Robot_RxChar(VCU_DATA_BUFFER[i]);
    }
    
}

void Robot::Parase()
{
    uint8_t mark[3] = {0};
    uint8_t msg[3] = {0};
    uint8_t *buff = NULL;

    if((this->queue.GetSize() >= VCU_PROTO_LEN*DecodeVCUSize) && ((this->queue.GetState() == RecQueue_ok) || (this->queue.GetState() == RecQueue_full)))
    {
        if(this->match)
        {
            for(uint32_t index = 0; index < this->queue.GetSize(); index ++)
            {
               this->queue.Check(index, mark, sizeof(mark));

                if((mark[0] == VCU_FRAME_HEAD_0) && (mark[2] == VCU_FRAME_HEAD_2))
                {
                    this->match = false;
                    buff = (uint8_t *)malloc(index);
                    this->queue.Pop(buff, index);
                    free(buff);

                    printf("vcu Frame Align match pos : %d\r\n", index);
                    break;
                }
                else
                {
                    uint8_t  data_delete[2] = {0};
                    this->queue.Pop(data_delete, 1);
                    this->queue.Check(0, msg, sizeof(msg));
                }
            }
        }
        else
        {
            this->queue.Check(0, msg, sizeof(msg));
            if((msg[0] == VCU_FRAME_HEAD_0) && 
                (msg[2] == VCU_FRAME_HEAD_2))
            {
                Decode();
            }
            else
            {
                uint8_t  data_delete[2] = {0};
                this->queue.Pop(data_delete, 1);
            }
        }
    }
}

int Robot::Proto_INSP_UnPack_Robot_RxChar(uint8_t* buf)
{
    uint8_t check_sum = 0;

    uint16_t crcValue = Common::calculateModbusCRC(buf, VCU_PROTO_LEN-2);
    uint16_t _value = buf[VCU_PROTO_LEN-1]*256+buf[VCU_PROTO_LEN-2];

    //循环打印buf的值
    string s;
    stringstream ss;
    for(int i = 0; i < VCU_PROTO_LEN; i++)
    {
        //buf[i] 十六进制形式存到s


        ss << hex << (int)buf[i] << " ";
        
    }
    s = ss.str();
    SEED_EXAMPLE_LOG1(SD_LOG_DEBUG,"buf %s ", s.c_str());
    if(crcValue == _value)
    {
        switch (buf[1])
        {
            case VCU_TYPE_1:
                    {
                        memcpy(&m_400_Type.Data,&buf[3],sizeof (VCU_400_TypeDef));

                        //机器人发送状态给导航
                        _Robot_Data_From_Vcu.vcuData.leftWheel = m_400_Type.bit.leftWheel;
                        _Robot_Data_From_Vcu.vcuData.leftWheel = m_400_Type.bit.rightWheel;
                        _Robot_Data_From_Vcu.vcuData.mpump = m_400_Type.bit.mpump;
                        _Robot_Data_From_Vcu.vcuData.swingArm = m_400_Type.bit.swingArm;
                        
                        _Robot_Data_From_Vcu.vcuData.emergency =  m_400_Type.bit.emergency;
                        _Robot_Data_From_Vcu.vcuData.mainLight =  m_400_Type.bit.mainLight;
                        _Robot_Data_From_Vcu.vcuData.leftLight =  m_400_Type.bit.leftLight;
                        _Robot_Data_From_Vcu.vcuData.rightLight =  m_400_Type.bit.rightLight;
                        _Robot_Data_From_Vcu.vcuData.alert =  m_400_Type.bit.alert;

                        //
                        _Robot_Data_From_Vcu.vcuData.mfLeft =  m_400_Type.bit.mfLeft;
                        _Robot_Data_From_Vcu.vcuData.mfUp =  m_400_Type.bit.mfUp;
                        _Robot_Data_From_Vcu.vcuData.mfRight =  m_400_Type.bit.mfRight;
                        _Robot_Data_From_Vcu.vcuData.mPush =  m_400_Type.bit.mPush;
                        _Robot_Data_From_Vcu.vcuData.mfFan =  m_400_Type.bit.mfFan;
                        _Robot_Data_From_Vcu.vcuData.autoSpraying =  m_400_Type.bit.autoSpraying;
                        _Robot_Data_From_Vcu.vcuData.automow =  m_400_Type.bit.automow;

                    }
                break;
            case VCU_TYPE_3:
                    {
                        memcpy(&m_403_Type.Data,&buf[3],sizeof (VCU_403_TypeDef));

                        //机器人发送状态给导航
                        _Robot_Data_From_Vcu.vcuData.deviceCode = m_403_Type.bit.deviceCode;
                        _Robot_Data_From_Vcu.vcuData.errorCode = m_403_Type.bit.errorCode;
                    }
                break;
            case VCU_TYPE_5:
                    {
                        

                    }
                break;
            case VCU_TYPE_6:
                    {
                         memcpy(&m_406_Type.Data,&buf[3],sizeof (VCU_406_TypeDef));
                        //机器人发送状态给导航
                        _Robot_Data_From_Vcu.bmsData.hbc = m_406_Type.bit.hbc;
                        _Robot_Data_From_Vcu.bmsData.hbv = m_406_Type.bit.hbv;
                        _Robot_Data_From_Vcu.bmsData.hbpc = m_406_Type.bit.hbpc*10;
                        _Robot_Data_From_Vcu.bmsData.hbcc = m_406_Type.bit.hbcc*10;
                        _Robot_Data_From_Vcu.bmsData.lbc = m_406_Type.bit.lbc;
                        _Robot_Data_From_Vcu.bmsData.lbv = m_406_Type.bit.lbv;
                        _Robot_Data_From_Vcu.bmsData.lbpc = m_406_Type.bit.lbpc*10;
                        _Robot_Data_From_Vcu.bmsData.lbcc = m_406_Type.bit.lbcc*10;
                    }
                break;
            
                
            }
        }
        else
        {
        }
    return 0;
}

ReportData Robot::GetReportData()
{
    return _Robot_Data_From_Vcu;
}




void Robot::SendDataToVcu()
{
    uint8_t* buf = new uint8_t[13];
    buf[0]=0x55;
    buf[1]=0x00;
    buf[2]=0x04;
    
    //导航发数据给机器人
    //m_401_Type.bit.leftWheel = _Server_Robot_Data.vcuData.leftWheel;
    //m_401_Type.bit.rightWheel = _Server_Robot_Data.vcuData.leftWheel;
    m_401_Type.bit.leftWheel = leftWheel_;//这两个是自己给 服务器是给最大速度
    m_401_Type.bit.rightWheel = rightWheel_;
    m_401_Type.bit.mpump = _Server_Robot_Data.vcuData.mpump;
    m_401_Type.bit.swingArm = _Server_Robot_Data.vcuData.swingArm;
    
    m_401_Type.bit.emergency = _Server_Robot_Data.vcuData.emergency;
    m_401_Type.bit.mainLight = _Server_Robot_Data.vcuData.mainLight;
    m_401_Type.bit.leftLight = _Server_Robot_Data.vcuData.leftLight;
    m_401_Type.bit.rightLight = _Server_Robot_Data.vcuData.rightLight;
    m_401_Type.bit.alert = _Server_Robot_Data.vcuData.alert ;
    m_401_Type.bit.Status_reservation = 0;
    //
    m_401_Type.bit.mfLeft = _Server_Robot_Data.vcuData.mfLeft;
    m_401_Type.bit.mfUp = _Server_Robot_Data.vcuData.mfUp;
    m_401_Type.bit.mfRight = _Server_Robot_Data.vcuData.mfRight;
    m_401_Type.bit.mPush = _Server_Robot_Data.vcuData.mPush;
    m_401_Type.bit.mfFan = _Server_Robot_Data.vcuData.mfFan;
    m_401_Type.bit.autoSpraying = _Server_Robot_Data.vcuData.autoSpraying;
    m_401_Type.bit.automow = _Server_Robot_Data.vcuData.automow;
    m_401_Type.bit.action_reservation = 0;

    memcpy(&buf[3],&m_401_Type.Data,sizeof (VCU_400_TypeDef));
    //计算校验和
    uint16_t crcValue = Common::calculateModbusCRC(buf, VCU_PROTO_LEN-2);
    buf[VCU_PROTO_LEN-1] = crcValue>>8;
    buf[VCU_PROTO_LEN-2] = crcValue&0xff;
    //循环打印buf的值
    string s;
    stringstream ss;
    for(int i = 0; i < VCU_PROTO_LEN; i++)
    {
        //buf[i] 十六进制形式存到s
        ss << hex << (int)buf[i] << " ";
    }
    s = ss.str();
    SEED_EXAMPLE_LOG1(SD_LOG_DEBUG,"sendbuf %s ", s.c_str());
    //清空sendbuffer
    memset(sendbuffer,0,VCU_PROTO_LEN);
    memcpy(sendbuffer, buf, VCU_PROTO_LEN);
}

void Robot::SportsStrategy()
{
    Gps_Data cur_gps_data;
    cur_gps_data.lon = vx1000_gnss_data.GnssLon;
    cur_gps_data.lat = vx1000_gnss_data.GnssLat;

    _Robot_Data_From_Vcu.lat = to_string(cur_gps_data.lat);
    _Robot_Data_From_Vcu.lon = to_string(cur_gps_data.lon);
    _Robot_Data_From_Vcu.yaw = 360-vx1000_gnss_data.GnssTraceAngle;
    //还没想好怎么判断error方便
    if(Common::_RobotError.bit.RTK_Connect_Error)
        _Robot_Data_From_Vcu.error = 1;
    //根据当前的状态进行运动
    if(_Task_Type == TaskType_Start)
    {
        //开始运动
        //获取当前点的GNSS经纬度信息
        
        //判断当前是否在任务点上
        if(Common::MygetDistance(cur_gps_data,Common::_RobotTask.CurrentPos)<0.2)
        {
            SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----开始转向-----");
            //计算当前位置和下一个点的角度
            double angle = Common::getAngle(cur_gps_data,Common::_RobotTask.NextPos);
            //设置速度,计算角度差
            double angle_diff = angle-vx1000_gnss_data.GnssHeading;
            feedback = angle_diff;
            double output;
            if(b_pid_start==false)
            {
                
                //角速度等于线速度的差除以0.9
                //PID算法矫正角速度
            
                b_pid_start=true;
                //启动PID
                setAngle  = angle_diff;
                SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----PID启动-----");
                output = AnglePid.compute(setAngle, 0, dt);
            }
            else
                output = AnglePid.compute(setAngle, angle_diff, dt);

            
            // output 是调整的角度值
            SEED_EXAMPLE_LOG2(SD_LOG_INFO, "-----Control %f--Feedback %f---",output,feedback);
            //发送数据给机器人
            if(output/0.9>max_angular_speed)
                setAngle_Speed = max_angular_speed;
            else
                setAngle_Speed = output;

            double VSpeed = setAngle_Speed * Robot_diameter;//获取线速度
            if(angle_diff>0.5)
            {
                leftWheel_ = VSpeed;
                rightWheel_ = 0;
            }
            else if(angle_diff<-0.5)
            {
                leftWheel_ = 0;
                rightWheel_ = VSpeed;
            }
            else
            {
                //
                leftWheel_ = VSpeed;
                rightWheel_ = VSpeed;
                b_pid_start=false;
                SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----PID结束,开始直行-----");
            }
            
        }
        else if(Common::MygetDistance(cur_gps_data,Common::_RobotTask.NextPos)<0.2)
        {
            SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----更新点信息-----");
            Common::_RobotTask.CurrentPos = Common::_RobotTask.NextPos;
            Common::_RobotTask.CurrentTaskID = Common::_RobotTask.NextTaskID;
            Common::_RobotTask.NextTaskID = to_string(atoi(Common::_RobotTask.CurrentTaskID.c_str())+1);
            Common::_RobotTask.NextPos = Common::_RobotTask._taskInfo.waypoint.find(Common::_RobotTask.NextTaskID)->second;
        }
        else
        {
            //判断是否偏航
            auto res =Common::isYaw(cur_gps_data,Common::_RobotTask.CurrentPos,Common::_RobotTask.NextPos,0.5);
            bool b_isYaw = std::get<0>(res);
            if (b_isYaw)
            {
                //偏航
                SEED_EXAMPLE_LOG2(SD_LOG_INFO, "---from %s-move to %s-偏航-----",Common::_RobotTask.CurrentPos,Common::_RobotTask.NextPos);
                _Robot_Data_From_Vcu.error = 302;
            }
            else
            {
                //不偏航
                SEED_EXAMPLE_LOG0(SD_LOG_INFO, "-----不偏航-----");
                //设置速度,计算角度差

                double angle = Common::getAngle(cur_gps_data,Common::_RobotTask.NextPos);
                //设置速度,计算角度差
                double angle_diff = angle-vx1000_gnss_data.GnssHeading;
                
                //发送数据给机器人
                if(angle_diff/0.9>max_angular_speed)
                    setAngle_Speed = max_angular_speed;
                else
                    setAngle_Speed = angle_diff;

                double VSpeed = setAngle_Speed * Robot_diameter;//获取线速度
                if(angle_diff>0.5)
                {
                    leftWheel_ = VSpeed;
                    rightWheel_ = VSpeed-angle_diff;
                }
                else if(angle_diff<-0.5)
                {
                    leftWheel_ = VSpeed-angle_diff;
                    rightWheel_ = VSpeed;
                }
                else
                {
                    //
                    leftWheel_ = VSpeed;
                    rightWheel_ = VSpeed;
                }
                SEED_EXAMPLE_LOG2(SD_LOG_INFO, "---左轮%d---右轮%d----",leftWheel_,rightWheel_);
            }
            
            
            

        }
    }
    else if(_Task_Type == TaskType_Stop)
    {
        //停止运动
        //
        leftWheel_ = 0;
        rightWheel_ = 0;
    }
    else if(_Task_Type == TaskType_Pause)
    {
        //暂停运动
        //
        leftWheel_ = 0;
        rightWheel_ = 0;
    }
    
}