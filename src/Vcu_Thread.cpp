#include "Vcu_Thread.h"
#include "Common.h"

using namespace std;

Vcu_Thread::Vcu_Thread(Rec_Queue &q) :  queue(q)
{
    
    
    try
    {
        /* code */
        this->port_name = Common::_system_setting.vcu_com_port_name;
        this->baudrate = 115200;
        this->Serial_Open();
    }
    catch(const std::exception& e)
    {
        //std::cerr << e.what() << '\n';
        
        SEED_EXAMPLE_LOG2(SD_LOG_ERROR, "-----串口%s打开失败[%s]-----",Common::_system_setting.vcu_com_port_name.c_str(),e.what());
        this->Serial_Open_State=false;
        Common::_RobotError.bit.RTK_Connect_Error=true;
    }
    
    
}

Vcu_Thread::~Vcu_Thread()
{
    try
    {
        _serial.flushInput();
        _serial.flushOutput();
        _serial.close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    
}
bool Vcu_Thread::GetOpenState()
{
    return this->Serial_Open_State;
}

bool Vcu_Thread::Serial_Open()
{
	_serial.setPort(this->port_name);
	_serial.setBaudrate(this->baudrate);
    // _serial.setParity(serial::parity_none);
    // _serial.setBytesize(serial::eightbits);
    // _serial.setStopbits(serial::stopbits_one);

	serial::Timeout to = serial::Timeout::simpleTimeout(1000);
	_serial.setTimeout(to);

	_serial.open();

	if (!_serial.isOpen())
	{
        SEED_EXAMPLE_LOG0(SD_LOG_ERROR,"VCU 串口打开失败")
		Serial_Open_State = false;
        return false;
	}
    _serial.setBreak(false);
    
    _serial.setFlowcontrol(serial::flowcontrol_none);
    SEED_EXAMPLE_LOG0(SD_LOG_INFO,"VCU 串口打开成功")
	Serial_Open_State = true;
	return true;
}


void Vcu_Thread::SerialRec_Thread()
{
    string rec_tmp;

    rec_tmp.clear();
    uint16_t i = 0;

    // while (1)
    // {
        if (_serial.available())
        {
            try
            {
                rec_tmp =_serial.read(_serial.available());
            }
            catch (std::out_of_range const &exc)
            {
                std::cout << exc.what() << std::endl;
                // continue;
            }

            if(rec_tmp.size() > 0)
            {
                SEED_EXAMPLE_LOG1(SD_LOG_INFO, "rec_tmp.size() = %d", rec_tmp.size());
                std::string hex_string;
                for (char c : rec_tmp) {
                    std::stringstream ss;
                    ss <<  std::setw(2) << std::setfill('0')<<std::hex << (unsigned int) (unsigned char)(c)<<" ";
                    hex_string += ss.str();
                }
                printf("rec_tmp:\t%s \t \r\n", hex_string.c_str());
               if(((this->queue.GetState() == RecQueue_ok) || (this->queue.GetState() ==RecQueue_empty )))
                {
                    this->queue.Push((uint8_t *)(rec_tmp.c_str()), rec_tmp.size());
                }
                else
                {
                    printf("queue state:\t%d\r\n", this->queue.GetState());
                    printf("RecQueue_ok\t\t----%d\r\n", RecQueue_ok);
                    printf("RecQueue_overflow_w\t----%d\r\n", RecQueue_overflow_w);
                    printf("RecQueue_overflow_r\t----%d\r\n", RecQueue_overflow_r);
                    printf("RecQueue_empty\t\t----%d\r\n", RecQueue_empty);
                    printf("RecQueue_full\t\t----%d\r\n", RecQueue_full);
                    printf("RecQueue_obj_error\t----%d\r\n", RecQueue_obj_error);

                    printf("queue size :\t%d\r\n", this->queue.GetSize());
                    printf("queue head:\t%d\r\n",this->queue.Head());
                    printf("queue end:\t%d\r\n",this->queue.End());

                    printf("queue push failed Thread Halt\r\n");
                    while(1);
                }
            }

            rec_tmp.clear();
        }
    // }
}

void Vcu_Thread::SerialSend_Thread()
{
    
    try
    {
        _serial.write(Robot::getInstance()->sendbuffer,VCU_PROTO_LEN);
    }
    catch (std::out_of_range const &exc)
    {
        std::cout << exc.what() << std::endl;
        // continue;
    }
    
}