#include "Rec_Queue.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

Rec_Queue::Rec_Queue(uint8_t *p_buff,uint16_t full_size)
{
    if(p_buff)
    {
        this->buff = p_buff;
        this->lenth = full_size;

        memset(this->buff, NULL, this->lenth);

        this->init_state = true;
        this->state = RecQueue_empty;

        printf("queue init done\r\n");
    }
}

Rec_Queue::~Rec_Queue()
{
}

bool Rec_Queue::Reset()
{
    if(this->init_state)
    {
        memset(this->buff, NULL, this->lenth);

        this->size = 0;
        this->head_pos = 0;
        this->end_pos = 0;

        this->init_state = true;
        this->state = RecQueue_empty;

        return true;
    }

    return false;
}

RecQueue_state Rec_Queue::UpdateState()
{
    if ((this->head_pos == this->end_pos) || (this->size == 0))
    {
        this->state = RecQueue_empty;
        return RecQueue_empty;
    }

    if (((this->end_pos + 1) % this->lenth) == this->head_pos)
    {
        this->state = RecQueue_full;
        return RecQueue_full;
    }

    this->state = RecQueue_ok;
    return this->state;
}

RecQueue_state Rec_Queue::Push(uint8_t *data, uint16_t size)
{
    if (this->lenth == 0)
        return RecQueue_obj_error;

    if ((this->state == RecQueue_ok) || (this->state == RecQueue_empty))
    {
        if (size <= (this->lenth - this->size))
        {
            for (uint16_t i = 0; i < size; i++)
            {
                this->end_pos %= this->lenth;
                this->buff[this->end_pos] = data[i];
                this->end_pos++;

                this->size++;

                this->UpdateState();
            }
        }
        else
            return RecQueue_overflow_w;
    }

    return this->state;
}

RecQueue_state Rec_Queue::Pop(uint8_t *data, uint16_t size)
{
    if (this->lenth == 0)
        return RecQueue_obj_error;

    if ((this->state == RecQueue_ok) || (this->state == RecQueue_full))
    {
        if (size <= this->size)
        {
            for (uint16_t i = 0; i < size; i++)
            {
                this->head_pos %= this->lenth;
                data[i] = this->buff[this->head_pos];
                this->buff[this->head_pos] = NULL;
                this->head_pos++;

                this->size--;

                this->UpdateState();
            }
        }
        else
            return RecQueue_overflow_r;
    }

    return this->state;
}

bool Rec_Queue::Check(uint16_t index, uint8_t *data, uint16_t size)
{
    if ((size == 0) || (data == NULL))
        return false;

    for (uint8_t i = 0; i < size; i++)
    {
        data[i] = this->buff[(this->head_pos + index + i) % this->lenth];
    }

    return true;
}

RecQueue_state Rec_Queue::GetState()
{
    return this->state;
}

uint16_t Rec_Queue::GetSize()
{
    return this->size;
}

uint16_t Rec_Queue::Head()
{
    return this->head_pos;
}

uint16_t Rec_Queue::End()
{
    return this->end_pos;
}

